<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MobilMania WebShop</title>

    <!--BOOTSTRAP-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"> </script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!---->
    <script src="https://kit.fontawesome.com/4a15fc06a1.js" crossorigin="anonymous"></script>

    <link href="style/css.css" rel="stylesheet">
    <link href="style/onama.css" rel="stylesheet">

    <script src="js/javascript.js"></script>
</head>

<body>

<?php include('navbar.php'); ?>
    <br>
    <br><br><br>
    <div class="container">
        <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">iPhone mobiteli</li>
            </ol>
        </nav>
    </div><br>
<div class="container">
    <br><br><br>
    <h1>O uređaju</h1>
    <br>
    <div class="row">
        <div class="col-lg-6">
            <img src="images/iphone13kamera.jpg" width="600px" height="300px" class="rounded">
        </div>
        <div class="col-lg-6">
            <h2><b>Najbolji sustav kamera ikada!</b></h2>
            <p>Sustav kamera dobiva najveću nadogradnju ikada. iPhone 13 Pro i iPhone 13 Pro Max koriste superinteligentni softver za nove tehnike fotografiranja i snimanja filmova. Po prvi puta Appel je dodao makro snimanje na svim stražnjim kamerama, te također podržava noćni mod (Night Mode). Kamere imaju i 77mm telefoto s 3x optičkim povećanje, te naravno ima glavnu i širokokutnu kameru. Učini svoje fotografije prosefionalnima i iskaži svoju kreativnu stranu!</p>
        </div>
    </div>
    <br><br><br>
    <div class="row">
        <div class="col-lg-6">
          <br><br><br>
            <h2><b>Postani profesionalni snimatelj</b></h2>
            <p>Sustav kamera dobiva najveću nadogradnju ikada. iPhone 13 Pro i iPhone 13 Pro Max koriste superinteligentni softver za nove tehnike fotografiranja i snimanja filmova. Po prvi puta Appel je dodao makro snimanje na svim stražnjim kamerama, te također podržava noćni mod (Night Mode). Kamere imaju i 77mm telefoto s 3x optičkim povećanje, te naravno ima glavnu i širokokutnu kameru. Učini svoje fotografije prosefionalnima i iskaži svoju kreativnu stranu!</p>
        </div>
        <div class="col-lg-6">
            <img src="images/iphone13video.png" width="650px" height="400px" class="rounded">
        </div>
    </div>
    <div class="row">
      <div class="col-lg-6">
          <img src="images/iphone13selfi.png" width="600px" height="400px" class="rounded">
      </div>
      <div class="col-lg-6">
        <br><br><br>
          <h2><b>Tvoji selfi će izgledati neodoljivo</b></h2>
          <p>A15 Bionic i kamera TrueDepth također omogućuju Face ID, najsigurniju autentifikaciju lica na pametnom telefonu. Predanja kamera će te ostaviti bez teksta, tvoji selfiji će izgledati neodoljivo i sjajno!</p>
      </div>
  </div>
</div>
<br><br><br>  
<div class="container">
    <div class="col-lg-12">
    <h1>Tehničke specifikacije</h1>        
    <table class="table table-light table-hover">
      <tbody>
        <tr>
          <td>Zaslon</td>
          <td>6.7 -inčni OLED Display</td>
        </tr>
        <tr>
          <td>Kamera</td>
          <td>12 MP, f/1.5, 26mm (wide), 1.9µm, dual pixel PDAF, sensor-shift OIS
            12 MP, f/2.8, 77mm (telephoto), PDAF, OIS, 3x optical zoom
            12 MP, f/1.8, 13mm, 120˚ (ultrawide), PDAF + Cinematic mode i Night mode
            TOF 3D LiDAR scanner (depth)</td>
        </tr>
        <tr>
            <td>Procesor</td>
            <td>Apple A15 Bionic (5 nm)</td>
        </tr>
        <tr>
          <td>Memorija</td>
          <td>128GB 6GB RAM, 256GB 6GB RAM, 512GB 6GB RAM, 1T 6GB RAM</td>
        </tr>
        <tr>
          <td>Baterija</td>
          <td>Fast charging 20W, 50% u 30 min 	</td>
        </tr>
      </tbody>
    </table>
    </div>
</div>
  
<a class="back-to-top"><i class="fas fa-arrow-up"></i></a>

      <!--https://w3schoolweb.com/bootsrap5-footer-with-social-media-icons/-->
      <section class="footer">
        <div class="container">
          <div class="footer__content">
            <div class="footer__heading">
              <h2>MobilMania</h2>
            </div>
            <p class="mb-0">Copyright &copy; 2021 mato3089@gmail.com</p>
       
            <ul class="social__media">
              <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></li>
              <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
            </ul>
          </div>
        </div>
      </section>
      
</body>
</html>