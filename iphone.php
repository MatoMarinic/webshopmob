<?php
include_once 'connection.php';
session_start();
?>

<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MobilMania WebShop</title>

    <!--BOOTSTRAP-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"> </script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!---->
    <script src="https://kit.fontawesome.com/4a15fc06a1.js" crossorigin="anonymous"></script>

    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.13.0/themes/base/jquery-ui.css">

    <link href="style/css.css" rel="stylesheet">

    <script src="addToCart.js"></script>

    <script src="filter.js"></script>
    <script src="priceRange.js"></script>

    <link href="style/onama.css" rel="stylesheet">

    <script src="js/javascript.js"></script>
</head>

<body>
<?php include('navbar.php'); ?><br><br><br><br>
<div class="container">
        <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">iPhone mobiteli</li>
            </ol>
        </nav>
    </div><br>
<div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
                <div id="filter" class="p-2 ms-md-4 ms-sm-2 " style="border: 1px solid black; border-radius:10px;">
                    <div class="border-bottom h5 text-uppercase text-center ">Filter</div>
                    <div class="box border-bottom">

                        <div class="box-label text-uppercase d-flex align-items-center">Cijena [kn]</div>
                        <input type="hidden" id="hidden_minimum_price" value="0">
                        <input type="hidden" id="hidden_maximum_price" value="15000">
                        <p id="price_show">0 - 15000
                        </p>
                        <div id="price_range"></div>
                    </div>
                    <br>

                   
                </div>

                <div class="col-md-9">
                    <br>
                    <div class="row filter_data">

                    </div>
                </div>
            </div>
              <div class="col-md-9">
                <div class="row">
                 
                    <?php
                    $sql = "SELECT manufacturer, model, zaslon, procesor, ram, baterija, price, tip, id, image FROM products WHERE manufacturer='iPhone'";
                    $result = mysqli_query($conn, $sql);

                    if (mysqli_num_rows($result) > 0) {
                        $index_id = 0;
                        while ($row = mysqli_fetch_assoc($result)) {
                            echo '
                        <div class="col-lg-3 mb-4" id="product_' . $index_id . '">
                        <div class="card h-100">
                        <h4 class="card-header text-center"><span id="product_' . $index_id . '_manufacturer">' . $row['manufacturer'] . '</span></h4>
                        <p class="text-center">' . $row['model'] . '</p> 
                        <div class="card-body">
                        <img src="images/' . $row['image'] . '" class="img-responsive img-thumbnail" alt="image">
                        <p class="card-text" id="mob1"><br>
                        <ul>
                        <li>Zaslon: ' . $row['zaslon'] . '</li>
                        <li>Procesor: ' . $row['procesor'] . '</li>
                        <li>RAM: ' . $row['ram'] . '</li>
                        <li>Baterija: ' . $row['baterija'] . '</li>
                        </ul>   
                        <br>
                        <p class="text-center"><b><span id="product_' . $index_id . '_price">' . $row['price'] . '</span>,00 kn</b><br></p>';
                            if (isset($_SESSION['loggedin']) && $_SESSION['loggedin']) {
                                echo    '<hr>
                        <p class="text-center"><a class="addToCart" href="addToCart.php?id=' . $row['id'] . '" onclick="appendId(' . $row['id'] . ')"><button class="button-24" role="button">Dodaj u košaricu <i class="fas fa-arrow-right"></i></button></a></p>';
                            }
                            echo                            '</div>
                        </div>
                        </div>';
                            $index_id++;
                        }
                    } else {
                        echo "0 results";
                    }
                    mysqli_close($conn); ?>
                </div>
            </div>
        </div>
                </div>
    <br>

    <br><br><br><br><br>
    <br><br>
    <!-- Back to top button -->
    <a class="back-to-top"><i class="fas fa-arrow-up"></i></a>

    <!--https://w3schoolweb.com/bootsrap5-footer-with-social-media-icons/-->
    <section class="footer">
        <div class="container">
            <div class="footer__content">
                <div class="footer__heading">
                    <h2>MobilMania</h2>
                </div>
                <p class="mb-0">Copyright &copy; 2021 mato3089@gmail.com</p>

                <ul class="social__media">
                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></li>
                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                </ul>
            </div>
        </div>
    </section>

</body>

</html>
