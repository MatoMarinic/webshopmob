<?php
   session_start();
   include_once 'connection.php';
   
   $errorMsg = "";

   $username = $email = $message = "";
   $isFilled = true;
   
   if (isset($_POST['posalji'])) { 
       $username = $_POST['username'];
       $email = $_POST['email'];
       $message = $_POST['message'];
       $errorMsg = "";
   
   
       if (empty($_POST['username'])) {
           $errorMsg .= "Ime obavezno. ";
           $isFilled = false;
       }
       else $username = $_POST['username'];
   
       if (empty($_POST['email'])) {
           $errorMsg .= "Email obavezan";
           $isFilled = false;
       }
       else $email = $_POST['email'];
   
       if (empty($_POST['message'])) {
           $errorMsg .= "Poruka obavezna";
           $isFilled = false;
       }
       else $message = $_POST['message'];
  
       if($isFilled) {
           $query = "INSERT INTO contact (username, email, message) VALUES ('$username', '$email', '$message')";
           if (mysqli_query($conn, $query)) {
               header('Location: index.php');
           } else {
               echo "Error: " . $query . ":-" . mysqli_error($conn);
           }
           mysqli_close($conn);
       }
   }
   
?>
<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
    
        <title>MobilMania WebShop</title>
    
        <!--BOOTSTRAP-->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">
    
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"> </script>
    
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    
        <!---->
        <script src="https://kit.fontawesome.com/4a15fc06a1.js" crossorigin="anonymous"></script>
    
        <link href="style/onama.css" rel="stylesheet">
        <script src="js/javascript.js"></script>
</head>
<body>
<?php include('navbar.php'); ?>

<br><br><br>
    <div class="container">
        <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Kontakt</li>
            </ol>
        </nav>
    </div><br>

    <div class="container">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header bg-primary text-white"><i class="fa fa-envelope"></i> Kontaktirajte nas.
                    </div>
                    <div class="card-body">
                        <form method="post">
                            <div class="form-group">
                                <label for="username">Ime</label>
                                <input type="text" name="username" class="form-control" id="username" aria-describedby="emailHelp" placeholder="Unesite ime" required>
                            </div>
                            <div class="form-group">
                                <label for="email">Email adresa</label>
                                <input type="email" name="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Unesite email" required>
                                <small id="emailHelp" class="form-text text-muted">Nećemo dijeliti vašu adresu!</small>
                            </div>
                            <div class="form-group">
                                <label for="message">Poruka</label>
                                <textarea class="form-control" name="message" id="message" rows="6" required></textarea>
                            </div>
                            <div class="mx-auto"><br>
                            <button type="submit" class="btn btn-primary text-right" name="posalji" onclick="fun()">Pošalji</button></div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-4">
                <div class="card bg-light mb-3">
                    <div class="card-header bg-success text-white text-uppercase"><i class="fa fa-home"></i> Naša adresa</div>
                    <div class="card-body">
                        <p>Jegerova 13</p>
                        <p>31000 OSIJEK</p>
                        <p>HRVATSKA</p>
                        <p>Email : mobilmania@gmail.com</p>
                        <p>Tel. +385 98 943 213</p>
    
                    </div>
    
                </div>
            </div>
        </div>
    </div>

    <br><br><br><br><br>
<!--https://w3schoolweb.com/bootsrap5-footer-with-social-media-icons/-->
<section class="footer">
  <div class="container">
    <div class="footer__content">
      <div class="footer__heading">
        <h2>MobilMania</h2>
      </div>
      <p class="mb-0">Copyright &copy; 2021 mato3089@gmail.com</p>
 
      <ul class="social__media">
        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
        <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></li>
        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
        <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
      </ul>
    </div>
  </div>
</section>
</body>
</html>


<script>
function fun() {
  alert("Uspješno ste poslali poruku");
}
</script>