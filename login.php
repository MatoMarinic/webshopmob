<?php
include_once 'connection.php';

$msg = 'Unesite vaše podatke.';

session_start();
$_SESSION['email'] = "";

if (isset($_POST['login']) && !empty($_POST['username']) && !empty($_POST['password'])) {
    $sql = "SELECT * FROM korisnici";
    $q = mysqli_query($conn, $sql);

    while ($line = mysqli_fetch_array($q, MYSQLI_ASSOC)) {
        if ($_POST['username'] == $line['username'] && $_POST['password'] == $line['password']) {
            $_SESSION['loggedin'] = true;
            $_SESSION['email'] = $_POST['email'];
            $_SESSION['role'] = $line['role'];
            $_SESSION['username'] = $line['username'];
               
            header('Location: index.php');    
        }
        else {
            $msg = 'Uneseni podaci su krivi.';
        }
    }
}
if(!empty($_POST["remember"])) {
	setcookie ("username",$_POST["email"],time()+ 3600);
	setcookie ("lozinka",$_POST["password"],time()+ 3600);
	echo "Cookies Set Successfuly";
} else {
	setcookie("username","");
	setcookie("password","");
	echo "Cookies Not Set";
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
    
        <title>MobilMania WebShop</title>
    
        <!--BOOTSTRAP-->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">
    
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"> </script>
    
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    
        <!---->
        <script src="https://kit.fontawesome.com/4a15fc06a1.js" crossorigin="anonymous"></script>
    
        <link href="style/ponudacss.css" rel="stylesheet">
        <link href="style/logincss.css" rel="stylesheet">
        <script src="js/javascript.js"></script>
</head>
<body>
<?php include('navbar.php'); ?>
<br><br><br><br>  
    <div class="container">
      <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
          <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.php"><u>Home</u></a></li>
          <li class="breadcrumb-item active" aria-current="page">Login</li>
          </ol>
      </nav>
  </div>

    <div class="wrapper fadeInDown">
        <div id="formContent">
          <!-- Tabs Titles -->
      
          <!-- Icon -->
          <div class="fadeIn first">
            <i class="far fa-user"></i> Login
            <p>
                <?php
                    echo $msg;
                ?>
            </p>
          </div>
      
          <!-- Login Form -->
          <form method="POST">
            <input type="text" id="username" class="fadeIn second" name="username" placeholder="username">
            <input type="password" id="password" class="fadeIn third" name="password" placeholder="password">
            <input type="submit" class="fadeIn fourth" value="Log In" name="login"><br>
            <input class="form-check-input me-2" type="checkbox" value="" id="remember" name="remember" />
                      <label class="form-check-label" for="remember">
                        Zapamti me.
                      </label>
            
          </form>
      
          <!-- Remind Passowrd -->
          <div id="formFooter">
            <a class="underlineHover" href="registracija.php">Nemate račun? Registrirajte se!</a>
          </div>
        </div>
      </div>

    
<!--https://w3schoolweb.com/bootsrap5-footer-with-social-media-icons/-->
<section class="footer">
  <div class="container">
    <div class="footer__content">
      <div class="footer__heading">
        <h2>MobilMania</h2>
      </div>
      <p class="mb-0">Copyright &copy; 2021 mato3089@gmail.com</p>
 
      <ul class="social__media">
        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
        <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></li>
        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
        <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
      </ul>
    </div>
  </div>
</section>
</body>
</html>