<?php 

include_once 'connection.php';
session_start();
ob_start();

$error = "";
$errorPass = "";
$errorPassRepeat = "";
$success ="";


$username = $email = $password = $repeatedPass = $userRole = "";
$status = true;

if (isset($_POST['reg_user']) && !empty($_POST["password"]) && isset( $_POST['password'] )){ 
    $username = $_POST['username'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $repeatedPass = $_POST['repeatedPassword'];
    $userRole = "korisnik";
    $error = "";
    $errorPass = "";
    $errorPassRepeat = "";
    $loggedin = "";
    $success = "";

      if (mb_strlen($_POST["password"]) <= 8) {
          $errorPass = "Lozinka mora sadržavati barem 8 znakova!";
      }
      else if(!preg_match("#[0-9]+#", $password)) {
          $errorPass = "Lozinka mora sadržavati barem 1 broj!";
      }
      else if(!preg_match("#[A-Z]+#", $password)) {
          $errorPass = "Lozinka mora sadržavati barem jedno veliko slovo!";
      }
      else if(!preg_match("#[a-z]+#", $password)) {
          $errorPass = "Lozinka mora sadržavati barem jedno malo slovo";
      }
      else if(!preg_match("#[\W]+#",$password)) {
          $errorPass = "Lozinka mora sadržavati barem jedan specijalan znak!";
      } 
      else if (strcmp($password, $repeatedPass) !== 0) {
          $errorPassRepeat = "Lozinke se ne podudaraju!";
      }
  
    else {
        $userExists = "SELECT * FROM korisnici WHERE email='$email'";
        $doesExist = mysqli_query($conn, $userExists);
        if (mysqli_num_rows($doesExist) > 0) {
            $error = "Email već zauzet!";
            $status = false;
        }

        if ($status) {
            $query = "INSERT INTO korisnici (username, email, password, role) VALUES ('$username', '$email', '$password', '$userRole')";
            $results = mysqli_query($conn, $query);
            if (!$results) {
                printf("Error: %s\n", $conn->error);
            }
           
            $_SESSION['loggedin'] = true;
            $_SESSION['email'] = $email; // $username coming from the form, such as $_POST['username']
            $_SESSION['role'] = $userRole;
            $_SESSION['username'] = $username;                                  // something like this is optional, of course
            $success = "Uspješno ste se registrirali. Preusmjeravamo Vas na početnu stranicu!";

        }
    }

    
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MobilMania WebShop</title>

    <!--BOOTSTRAP-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"> </script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!---->
    <script src="https://kit.fontawesome.com/4a15fc06a1.js" crossorigin="anonymous"></script>

    <link href="style/onama.css" rel="stylesheet">
    <script src="js/javascript.js"></script>
    <style>
  
    </style>
</head>

<body>

    <?php include('navbar.php'); ?>
    <br><br><br>

    <div class="container">
        <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Registracija</li>
            </ol>
        </nav>
    </div>

    <section class="vh-100" style="background-color: #eee;">
        <div class="container h-100">
            <div class="row d-flex justify-content-center align-items-center h-100">
                <div class="col-lg-12 col-xl-11">
                    <div class="card text-black" style="border-radius: 25px;">
                        <div class="card-body p-md-5">
                            <div class="row justify-content-center">
                                <div class="col-md-10 col-lg-6 col-xl-5 order-2 order-lg-1">

                                    <p class="text-center h1 fw-bold mb-5 mx-1 mx-md-4 mt-4">Registracija</p>

                                    <form class="mx-1 mx-md-4" method="POST">

                                        <div class="d-flex flex-row align-items-center mb-4">
                                            <i class="fas fa-user fa-lg me-3 fa-fw"></i>
                                            <div class="form-outline flex-fill mb-0">
                                                <input type="text" id="username" class="form-control" name="username" required/>
                                                <label class="form-label" for="username">Vaše ime</label>
                                            </div>
                                        </div>

                                        <div class="d-flex flex-row align-items-center mb-4">
                                            <i class="fas fa-envelope fa-lg me-3 fa-fw"></i>
                                            <div class="form-outline flex-fill mb-0">
                                                <input type="email" id="email" class="form-control" name="email" required/>
                                                <label class="form-label" for="email">Vaš Email</label>
                                            </div>
                                        </div>

                                        <div class="d-flex flex-row align-items-center mb-4">
                                            <i class="fas fa-lock fa-lg me-3 fa-fw"></i>
                                            <div class="form-outline flex-fill mb-0">
                                                <input type="password" id="password" class="form-control"
                                                    name="password" required />
                                                <label class="form-label" for="password">Lozinka</label>
                                                <br>
                                               <?php echo'<p style="color:red">' .$errorPass. '</p>'?>
		
                                            </div>
                                        </div>

                                        <div class="d-flex flex-row align-items-center mb-4">
                                            <i class="fas fa-key fa-lg me-3 fa-fw"></i>
                                            <div class="form-outline flex-fill mb-0">
                                                <input type="password" id="repeatedPassword" class="form-control"
                                                    name="repeatedPassword" />
                                                <label class="form-label" for="repeatedPassword">Ponovite
                                                    lozinku</label>
                                                    <br>
                                                    <?php echo'<p style="color:red">' .$errorPassRepeat. '</p>'?>
                                            </div>
                                        </div>

                                        <div class="d-flex justify-content-center mx-4 mb-3 mb-lg-4">
                                            <button type="submit" class="btn btn-primary btn-lg"
                                                name="reg_user">Registriraj me</button>
                                                
                                        </div>
                                    </form>
                                    <div class="d-flex justify-content-left mx-4 mb-3 mb-lg-4">
                                    <p><b>Napomena! </b>Lozinka mora sadržavati:<br>
                                        jedno malo slovo - [a-z],
                                        <br>
                                        jedno veliko slovo - [A-Z],
                                        <br>
                                        jedan broj - [1-9],
                                        <br>
                                        jedan specijalan znak - [!,@,$,#]
                                                
                                    </div>
                                    <br>
                                    <div class="d-flex justify-content-center mx-4 mb-3 mb-lg-4">
                                        <p><strong>Već imate korisnički račun? Prijavite se!</strong></p>
                                       <p><br><a href="login.php"><button style="margin-right:120px;" class="btn btn-primary">Prijava</button></a></p>
                                    
                                    </div>       
                                            
                                     
                                </div>
                                <div class="col-md-10 col-lg-6 col-xl-7 d-flex align-items-center order-1 order-lg-2">

                                    <img src="https://www.sccpss.com/registration/PublishingImages/register.jpg"
                                        class="img-fluid" alt="Sample image">
                                        
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <!--https://w3schoolweb.com/bootsrap5-footer-with-social-media-icons/-->
    <section class="footer">
        <div class="container">
            <div class="footer__content">
                <div class="footer__heading">
                    <h2>MobilMania</h2>
                </div>
                <p class="mb-0">Copyright &copy; 2021 mato3089@gmail.com</p>

                <ul class="social__media">
                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></li>
                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                </ul>
            </div>
        </div>
    </section>

</body>
</html>


<?php if(isset($_SESSION['loggedin']) == true) header('Refresh: 5; URL=index.php'); 
      ob_end_flush();
?>