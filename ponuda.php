<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>MobilMania WebShop</title>

  <!--BOOTSTRAP-->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"> </script>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

  <!---->
  <script src="https://kit.fontawesome.com/4a15fc06a1.js" crossorigin="anonymous"></script>

  <link href="style/ponudacss.css" rel="stylesheet">

  <script src="js/javascript.js"></script>
</head>

<body>
<?php include('navbar.php'); ?>

  <br><br><br>

  <!-- Page Content -->
  <div class="container">
    <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Ponuda mobitela</li>
      </ol>
    </nav>
  </div>

  <!--OVDJE DODATI GALERIJU SLIKA I BUTTON-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-2 mb-4">
        <div class="card h-100 filtar">
          <h4 class="card-header">
            Filtriraj</h4>
          <div class="card-body">
            <p id="proizvodac_p"><b>Proizvođač</b></p>
            <form action="">
              <label for="proizvodac1" class="checkbox">
                <input type="checkbox" id="proizvodac1" name="proizvodac1">
                <span>Samsung</span></label>
              <br>
              <label for="proizvodac2" class="checkbox">
                <input type="checkbox" id="proizvodac2" name="proizvodac2">
                <span>iPhone</span>
              </label><br>

              <label for="proizvodac3" class="checkbox">
                <input type="checkbox" id="proizvodac3" name="proizvodac3">
                <span>Motorola</span>
              </label><br>

              <label for="proizvodac4" class="checkbox">
                <input type="checkbox" id="proizvodac4" name="proizvodac4"><span>Huawei</span></label><br>

              <label for="proizvodac5" class="checkbox">
                <input type="checkbox" id="proizvodac5" name="proizvodac5"><span>Xiaomi</span></label>

            </form>
            <hr>
            <p id="proizvodac"><b>Prikaži prvo</b></p>
            <form action="">

              <label for="najskuplji" class="radiobox">
                <input type="radio" id="najskuplji" name="filtar_mob"><span>Najskuplji</span></label><br>

              <label for="najjeftiniji" class="radiobox"> <input type="radio" id="najjeftiniji"
                  name="filtar_mob"><span>Najjeftiniji</span></label><br>

            </form>
            <hr>
          </div>
        </div>
      </div>

      <div class="col-lg-3 mb-4">
        <div class="card h-100 card_mob1">
          <h4 class="card-header">
            iPhone</h4>
          <p>&nbsp;<b>iPhone 13 Pro Max</b></p>
          <div class="card-body">
            <a href="../HTML/iPhone13.php"><img src="images/iphone13.jpg" width="350px" height="350px"></a>

            <p class="card-text" id="mob1"><br>
              &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;<b>6.900,00 kn</b><br></p>
            <hr>
            <p class="card-text" id="mob1">&nbsp; &nbsp;&nbsp;<b>Super cijena!</b> </p>
            <a href="../HTML/iPhone13.php"><button class="button-24" role="button" id="odaberi1">Odaberi <i
                  class="fas fa-arrow-right"></i></button></a>
          </div>
        </div>
      </div>

      <div class="col-lg-3 mb-4">
        <div class="card h-100 card_mob1">
          <h4 class="card-header">
            Samsung</h4>
          <p>&nbsp;<b>Samsung Galaxy A52s 5G</b></p>
          <div class="card-body">
            <a href="../HTML/samsunga52s.php"><img src="images/galaxya52s.jpg" width="350px" height="350px"></a>

            <p class="card-text" id="mob1"><br>
              &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;<b>2.490,00 kn</b><br></p>
            <hr>
            <p class="card-text" id="mob1">&nbsp; &nbsp;&nbsp;<b>Super cijena!</b> </p>
            <a href="../HTML/samsunga52s.php"><button class="button-24" role="button" id="odaberi1">Odaberi <i
                  class="fas fa-arrow-right"></i></button></a>
          </div>
        </div>
      </div>
      <div class="col-lg-4 mb-4">
        <div class="card h-100 card_mob1">
          <h4 class="card-header">
            Motorola</h4>
          <p>&nbsp;<b>Motorola E7 dual sim siva</b></p>
          <div class="card-body motoe7">
            <a href="../HTML/motoe7.php"><img src="images/motoe7.jpg" width="340px" height="350px"></a>

            <p class="card-text" id="mob1"><br>
              &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;<b>1.299,00 kn</b><br></p>
            <hr>
            <p class="card-text" id="mob1">&nbsp; &nbsp;&nbsp;<b>Super cijena!</b> </p>
            <a href="../HTML/motoe7.php"><button class="button-24" role="button" id="odaberi1">Odaberi <i
                  class="fas fa-arrow-right"></i></button></a>
          </div>
        </div>
      </div>

      <div class="col-lg-3 mb-4">
        <div class="card h-100 card_mob2">
          <h4 class="card-header">
            Huwaei</h4>
          <p>&nbsp;<b>Huawei P40 lite zeleni 128GB</b></p>
          <div class="card-body">
            <a href="../HTML/p40lite.php"><img src="images/huaweip40lite.jpg" width="340px" height="350px"></a>

            <p class="card-text" id="mob1"><br>
              &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;<b>1.898,00 kn</b><br></p>
            <hr>
            <p class="card-text" id="mob1">&nbsp; &nbsp;&nbsp;<b>Super cijena!</b> </p>
            <a href="../HTML/p40lite.php"><button class="button-24" role="button" id="odaberi1">Odaberi <i
                  class="fas fa-arrow-right"></i></button></a>
          </div>
        </div>
      </div>

      <div class="col-lg-3 mb-4">
        <div class="card h-100 card_mob2">
          <h4 class="card-header">
            Samsung</h4>
          <p>&nbsp;<b>Samsung Galaxy A03s crni</b></p>
          <div class="card-body">
            <a href="../HTML/a03s.php"><img src="images/a03s.jpg" width="350px" height="350px"></a>

            <p class="card-text" id="mob1"><br>
              &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;<b>1.199,00 kn</b><br></p>
            <hr>
            <p class="card-text" id="mob1">&nbsp; &nbsp;&nbsp;<b>Super cijena!</b> </p>
            <a href="../HTML/a03s.php"><button class="button-24" role="button" id="odaberi1">Odaberi <i
                  class="fas fa-arrow-right"></i></button></a>
          </div>
        </div>
      </div>

      <div class="col-lg-4 mb-4">
        <div class="card h-100 card_mob2">
          <h4 class="card-header">
            Xiaomi</h4>
          <p>&nbsp;<b>Xiaomi Mi 11 lite 5G 128GB sivi</b></p>
          <div class="card-body">
            <a href="../HTML/xiaomi11.php"><img src="images/xiamoi11.jpg" width="350px" height="350px"></a>

            <p class="card-text" id="mob1"><br>
              &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;<b>1.999,00 kn</b><br></p>
            <hr>
            <p class="card-text" id="mob1">&nbsp; &nbsp;&nbsp;<b>Super cijena!</b> </p>
            <a href="../HTML/xiaomi11.php"><button class="button-24" role="button" id="odaberi1">Odaberi <i
                  class="fas fa-arrow-right"></i></button></a>
          </div>
        </div>
      </div>


      <div class="col-lg-3 mb-4">
        <div class="card h-100 card_mob2">
          <h4 class="card-header">
            Samsung</h4>
          <p>&nbsp;<b>Samsung Galaxy S10+ 1TB DS crni</b></p>
          <div class="card-body">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="../HTML/samsungs10.php"><img src="images/s10plus.jpg"
                width="270px" height="350px"></a>

            <p class="card-text" id="mob1"><br>
              &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;<b>3.789,00 kn</b><br></p>
            <hr>
            <p class="card-text" id="mob1">&nbsp; &nbsp;&nbsp;<b>Super cijena!</b> </p>
            <a href="../HTML/samsungs10.php"><button class="button-24" role="button" id="odaberi1">Odaberi <i
                  class="fas fa-arrow-right"></i></button></a>
          </div>
        </div>
      </div>

      <div class="col-lg-3 mb-4">
        <div class="card h-100 card_mob2">
          <h4 class="card-header">
            iPhone</h4>
          <p>&nbsp;<b>iPhone 11 64GB sivi 2020</b></p>
          <div class="card-body">
            &nbsp; &nbsp;&nbsp;&nbsp;<a href="../HTML/iPhone11.php"><img src="images/iphone11.jpg" width="270px"
                height="350px"></a>

            <p class="card-text" id="mob1"><br>
              &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;<b>4.464,00 kn</b><br></p>
            <hr>
            <p class="card-text" id="mob1">&nbsp; &nbsp;&nbsp;<b>Super cijena!</b> </p>
            <a href="../HTML/iPhone11.php"><button class="button-24" role="button" id="odaberi1">Odaberi <i
                  class="fas fa-arrow-right"></i></button></a>
          </div>
        </div>
      </div>

      <div class="col-lg-4 mb-4">
        <div class="card h-100 card_mob2">
          <h4 class="card-header">
            Huawei</h4>
          <p>&nbsp;<b>Huawei Nova 8i plavi</b></p>
          <div class="card-body">
            <a href="../HTML/nova8i.php"><img src="images/nova8i.jpg" width="320px" height="350px"></a>

            <p class="card-text" id="mob1"><br>
              &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;<b>2.699,00 kn</b><br></p>
            <hr>
            <p class="card-text" id="mob1">&nbsp; &nbsp;&nbsp;<b>Super cijena!</b> </p>
            <a href="../HTML/nova8i.php"><button class="button-24" role="button" id="odaberi1">Odaberi <i
                  class="fas fa-arrow-right"></i></button></a>
          </div>
        </div>
      </div>

      <div class="col-lg-3 mb-4">
        <div class="card h-100 card_mob2">
          <h4 class="card-header">
            iPhone</h4>
          <p>&nbsp;<b>iPhone SE 64GB 2020 crni</b></p>
          <div class="card-body">
            <a href="../HTML/iPhoneSE.php"><img src="images/iphonese.jpg" width="350px" height="350px"></a>

            <p class="card-text" id="mob1"><br>
              &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;<b>2.999,00 kn</b><br></p>
            <hr>
            <p class="card-text" id="mob1">&nbsp; &nbsp;&nbsp;<b>Super cijena!</b> </p>
            <a href="../HTML/iPhoneSE.php"><button class="button-24" role="button" id="odaberi1">Odaberi <i
                  class="fas fa-arrow-right"></i></button></a>
          </div>
        </div>
      </div>

      <div class="col-lg-3 mb-4">
        <div class="card h-100 card_mob2">
          <h4 class="card-header">
            Samsung</h4>
          <p>&nbsp;<b>Samsung Galaxy S21 Ultra 256GB crni 5G</b></p>
          <div class="card-body">
            <a href="../HTML/s21ultra.php"><img src="images/s21ultra.jfif" width="350px" height="350px"></a>

            <p class="card-text" id="mob1"><br>
              &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;<b>7.999,00 kn</b><br></p>
            <hr>
            <p class="card-text" id="mob1">&nbsp; &nbsp;&nbsp;<b>Super cijena!</b> </p>
            <a href="../HTML/s21ultra.php"><button class="button-24" role="button" id="odaberi1">Odaberi <i
                  class="fas fa-arrow-right"></i></button></a>
          </div>
        </div>
      </div>

      <div class="col-lg-4 mb-4">
        <div class="card h-100 card_mob2">
          <h4 class="card-header">
            Xiaomi</h4>
          <p>&nbsp;<b>Xiaomi Poco X3 Pro 128GB crni</b></p>
          <div class="card-body">
            <a href="../HTML/xiaomipoco.php"> <img src="images/poco.jpg" width="350px" height="350px"></a>

            <p class="card-text" id="mob1"><br>
              &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;<b>1.750,00 kn</b><br></p>
            <hr>
            <p class="card-text" id="mob1">&nbsp; &nbsp;&nbsp;<b>Super cijena!</b> </p>
            <a href="../HTML/xiaomipoco.php"><button class="button-24" role="button" id="odaberi1">Odaberi <i
                  class="fas fa-arrow-right"></i></button></a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <br>

  <div class="container">
    <div class="d-flex justify-content-center">
      <a href="#" class="previous" style="pointer-events: none">&laquo; Prethodna</a>
      <a href="ponuda2.php" class="next">Sljedeća &raquo;</a>
    </div>
  </div>
  <a class="back-to-top"><i class="fas fa-arrow-up"></i></a>

  <br><br>
  <!--https://w3schoolweb.com/bootsrap5-footer-with-social-media-icons/-->
  <section class="footer">
    <div class="container">
      <div class="footer__content">
        <div class="footer__heading">
          <h2>MobilMania</h2>
        </div>
        <p class="mb-0">Copyright &copy; 2021 mato3089@gmail.com</p>

        <ul class="social__media">
          <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
          <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></li>
          <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
          <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
        </ul>
      </div>
    </div>
  </section>
</body>

</html>