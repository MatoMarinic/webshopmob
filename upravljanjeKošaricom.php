<?php

include_once 'connection.php';
session_start();
    if ($_SESSION['role'] != "admin"){
        header('Location: index.php'); 
    } 
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MobilMania WebShop</title>

    <!--BOOTSTRAP-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"> </script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <link rel="stylesheet" href="//cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">
    
    <script src="//cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>


    <!---->
    <script src="https://kit.fontawesome.com/4a15fc06a1.js" crossorigin="anonymous"></script>

    <link href="style/css.css" rel="stylesheet">

    <script src="js/javascript.js"></script>

    <script src="include.js"></script>
</head>
<body>

<?php include('navbar.php'); ?>
<br><br><br>
<main>
      <div class="container-fluid">
          <div class="row">
                <div class="mx-auto p-5 my-5">
                <header class="mb-5"><h1 class="text-center">Pregled košarice</h1></header>

                <a href="dashboard.php"><button class="button-24" role="button">Povratak</button></a>
                <p>
                <table class="table table-light table-hover" id="tablica" style="max-height: 100px; height:100%;">
                        <thead>
                            <tr>
                            <th scope="col">ID kupnje</th>
                            <th scope="col">ID kupca</th>
                            <th scope="col">Količina</th>
                            <th scope="col">Cijena(kn)</th>
                            <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                        $sql = "SELECT * FROM kosarica";
                        $q = mysqli_query($conn, $sql);

                        while($line = mysqli_fetch_array($q, MYSQLI_ASSOC)) {
                            echo '
                                    <tr>
                                        <td >' . $line['kupnja_id'] .'</td>
                                            <td>'. $line['sesija_id'] . '</td>
                                            <td>'. $line['quantity'] .'</td>
                                            <td>'. $line['price'] .'</td>
                                            <td>
                                            </td>
                                    </tr>';
                        }

                    echo' </tbody> </table>';
                            $conn->close();
                            ?>

                        
                </div>
          </div>
      </div>
</main> 
<script>
$(document).ready(function() {
    $('#tablica').DataTable();
} );
</script>
<section class="footer">
  <div class="container">
    <div class="footer__content">
      <div class="footer__heading">
        <h2>MobilMania</h2>
      </div>
      <p class="mb-0">Copyright &copy; 2021 mato3089@gmail.com</p>
 
      <ul class="social__media">
        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
        <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></li>
        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
        <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
      </ul>
    </div>
  </div>
</section>
</body>
</html>