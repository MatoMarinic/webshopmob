<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MobilMania WebShop</title>

    <!--BOOTSTRAP-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"> </script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!---->
    <script src="https://kit.fontawesome.com/4a15fc06a1.js" crossorigin="anonymous"></script>

    <link href="style/css.css" rel="stylesheet">
    <link href="style/onama.css" rel="stylesheet">

    <script src="js/javascript.js">

    </script>


<body>

<?php include('navbar.php'); ?>
    <br>
    <br><br><br>
    <div class="container">
        <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Huawei mobiteli</li>
            </ol>
        </nav>
    </div>

    <div class="container-fluid">
        <br><br><br>
        <h1>&nbsp;&nbsp;&nbsp;O uređaju</h1>
        <br>
        <div class="row bg-dark">
            <div class="col">
                <br>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="images/matexs1.jpg"
                    width="490px" height="350px" class="rounded">
            </div>
            <div class="col">
                <br>
                <br>
                <br>
                <br>
                <br>
                <h2 style="color: white;"><b>Pogledaj više jednim pogledom</b></h2>
                <p style="color: white;">Zapanjujući HUAWEI FullView zaslon nudi ogromne detalje i živopisne boje gdje god pogledaš, <br>dok ti izvanredan 8-inčni zaslon omogućava da vidiš više stvari odjednom. <br>Nema potrebe da se pomičeš naprijed/natrag - prekrasan krajolik,<br> uzbudljivi široki vidici i opsežna infografija otkrivaju se sami pred tvojim očima.</p>
            </div>
        </div>
        <div class="row bg-dark">
            <div class="col">
                <br>
                <br>
                <br>
                &nbsp;&nbsp;&nbsp; <h2 style="color: white; margin-left: 40px;"><b><br><br>Uhvati svaku posebnost
                </b></h2>
                <p style="color: white; margin-left: 40px;">Integrirajući stražnju i prednju kameru, 40 MP Supersensing Leica četverostruki sustav kamera omogućuje ti da zabilježiš sve ljepote svijeta koje se pojavljuju pred tvojim očima. Daleko ili blizu, tamno ili svijetlo, veliko ili malo, sada svaka vrijedna slika ostaje ne samo u tvom sjećanju, već i u tvom telefonu.
                </p>
            </div>
            <div class="col"><br><br>
                &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; <img src="images/matexs2.jpg" width="600px" height="400px"
                    class="rounded">
            </div>
        </div>
        <div class="row bg-dark">
            <br><br><br>
            <div class="col"><br><br>
                <br>
                <img src="images/matexs3.jpg" width="600px" height="350px" class="rounded" style="margin-left: 30px;">
            </div>
            <div class="col">
                <br>
                <br>
                <br>
                <br>
                <br>
                <h2 style="color: white;"><b>Ogledalo tvoje ljepote</b></h2>
                <p style="color: white;">Mirror Shooting s dvostrukim zaslonom omogućava tebi i onome tko te fotografira da u stvarnom vremenu provjerite i podijelite kreativne ideje kako biste pokazali svoju ljepotu baš onako kako vi to želite.
                </p>
            </div>
        </div>
        <div class="row bg-dark">
            <div class="col">
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <h2 style="color: white; margin-left: 40px;"><b>Predvodnik 5G ere</b></h2>
                <p style="color: white; margin-left: 40px;">Pogonjen Kirin 990 5G čipsetom, HUAWEI Mate Xs postiže vrhunsku brzinu i performanse u ovoj 5G eri. CPU arhitektura energetske učinkovitosti na tri razine donosi revolucionarne performanse uz manju potrošnju. Dual-core i Tiny-core NPU stvaraju čvrste temelje za eksplozivni razvoj budućih sposobnosti umjetne inteligencije.
                </p>
            </div>
            <div class="col"><br><br><br>
                &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; <img src="images/matexs4.jpg" width="550px" height="360px"
                    class="rounded"><br><br>
            </div>
        </div>
    </div>
</div>
    <br><br>

    <div class="container">
        <div class="col-lg-12">
            <h1>Tehničke specifikacije</h1>
            <table class="table table-light table-hover">
                <tbody>
                    <tr>
                        <td>Podržane mreže</td>
                        <td>GSM 850, LTE 1800, LTE 2600, 900, 1800, 1900, UMTS 850, UMTS 900, UMTS 1900, UMTS 2100, LTE
                            800 MHz</td>
                    </tr>
                    <tr>
                        <td>Prijenos podataka</td>
                        <td>GPRS, HSDPA 42.2, EDGE, LTE Cat20 2000 Mbps DL, 5G, UMTS, HSUPA, HSUPA 5.76
                        </td>
                    </tr>
                    <tr>
                        <td>Povezivost</td>
                        <td>Bluetooth, NFC, Sinkronizacija s racunalom, USB kabel, WLAN, GPS</td>
                    </tr>
                    <tr>
                        <td>Poruke</td>
                        <td>SMS, MMS, E-mail klijent</td>
                    </tr>
                    <tr>
                        <td>Zaslon</td>
                        <td>OLED; Sklopljen: 2480 x 2200 Rasklopljen: Glavni zaslon: 2480 x 1148 Sekundarni zaslon: 2480 x 892</td>
                    </tr>
                    <tr>
                        <td>Kamera</td>
                        <td>Leica četverostruka:40 Mpx+16 Mpx+8Mpx+TOF kamera; Selfie: nakon preklapanja stražnja udvostručena</td>
                    </tr>
                    <tr>
                        <td>Memorija telefona</td>
                        <td>8 GB RAM-a + 512 GB ROM</td>
                    </tr>
                    <tr>
                        <td>Procesor</td>
                        <td>HUAWEI Kirin 990 5G</td>
                    </tr>
                    <tr>
                        <td>Dimenzije uređaja</td>
                        <td>Širina: Sklopljen: 78,5 mm, Rasklopljen: 146,2 mm; Visina: 161,3 mm, Debljina: 11 mm, 5,4 mm mm</td>
                    </tr>
                    <tr>
                        <td>Težina</td>
                        <td>300 g</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <br>

    <a class="back-to-top"><i class="fas fa-arrow-up"></i></a>

    <!--https://w3schoolweb.com/bootsrap5-footer-with-social-media-icons/-->
    <section class="footer">
        <div class="container">
            <div class="footer__content">
                <div class="footer__heading">
                    <h2>MobilMania</h2>
                </div>
                <p class="mb-0">Copyright &copy; 2021 mato3089@gmail.com</p>

                <ul class="social__media">
                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></li>
                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                </ul>
            </div>
        </div>
    </section>

</body>

</html>