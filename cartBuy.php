<?php
include_once 'connection.php';

ob_start();
session_start();
$idSesije = session_id();
  
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MobilMania WebShop</title>

    <!--BOOTSTRAP-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"> </script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!---->
    <script src="https://kit.fontawesome.com/4a15fc06a1.js" crossorigin="anonymous"></script>

    <link href="style/css.css" rel="stylesheet">

    <script src="js/javascript.js"></script>

    <script src="include.js"></script>
</head>

<body>
<?php include('navbar.php'); ?>
<br><br><br><br><br><br><br>
<header class="mb-5"><h1 class="text-center">Kupnja izvršena</h1></header>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-5 mx-auto mt-5 p-4 bg-dark text-muted text-center">
                <?php 
                    $sql = "SELECT * FROM kosarica WHERE sesija_id = '$idSesije'";
                    $result = mysqli_query($conn, $sql);
                    while($data = mysqli_fetch_array($result)) {
                        echo
                        '<h1 class="text-white">Hvala na kupnji!</h1>
                        <p>Vaša sesija:  <span class="text-white">'.$data['sesija_id'].'</span></p>
                        <p>Količina:  <span class="text-white">'.$data['quantity'].'</span></p>
                        <p>Cijena:  <span class="text-white">'.$data['price'].' kn</span></p>';
                    }
                ?>
            </div>
        </div>
    </div>
</main>
<?php header('Refresh: 5; URL=index.php'); ?>
<p></p>
<p class="text-center" style="font-size:1.5em">Uskoro će te biti vraćeni na početnu stranicu!</p>
<section class="footer">
  <div class="container">
    <div class="footer__content">
      <div class="footer__heading">
        <h2>MobilMania</h2>
      </div>
      <p class="mb-0">Copyright &copy; 2021 mato3089@gmail.com</p>
 
      <ul class="social__media">
        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
        <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></li>
        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
        <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
      </ul>
    </div>
  </div>
</section>
</body>
</html>

<?php ob_end_flush(); ?>