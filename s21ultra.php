<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MobilMania WebShop</title>

    <!--BOOTSTRAP-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"> </script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!---->
    <script src="https://kit.fontawesome.com/4a15fc06a1.js" crossorigin="anonymous"></script>

    <link href="style/css.css" rel="stylesheet">
    <link href="style/onama.css" rel="stylesheet">

    <script src="js/javascript.js"></script>
</head>

<body>

<?php include('navbar.php'); ?>
    <br>
    <br><br><br>

    <div class="container">
    <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Pregled</li>
      </ol>
    </nav>
  </div>

<div class="container-fluid">
    <br><br><br>
    <h1>&nbsp;&nbsp;&nbsp;O uređaju</h1>
    <br>
    <div class="row bg-dark">
        <div class="col-lg-6">
            <br>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="images/s21ultra1.jpg" width="500px" height="350px" class="rounded"> 
        </div>
        <div class="col-lg-6">
            <br>
            <br>
            <br>
            <br>
            <br>
            <h2 style="color: white;"><b>Spektakularan. U svakom smislu</b></h2>
            <p style="color: white;">Predstavljamo Galaxy S21 Ultra 5G. Dizajniran s jedinstvenom kamerom izrezanoj po konturi kako bi stvorio revoluciju u svijetu fotografije i omogućio ti snimanje 8K videozapisa filmske kvalitete i spektakularnih fotografija, sve to u jednom potezu. A uz njegov dosad najbrži čip ugrađen u neki Galaxy uređaj, najčvršće zaštitno staklo, 5G brzine i bateriju s cjelodnevnim trajanjem, Ultra s lakoćom opravdava svoje ime.</p> 
        </div>        
    </div>
    <div class="row bg-dark">
        <div class="col-lg-6">
            <br>
            <br>
            <br>
            &nbsp;&nbsp;&nbsp; <h2 style="color: white; margin-left: 40px;"><b>Dizajniran da bude revolucionaran</b></h2>
            <p style="color: white; margin-left: 40px;">Predstavljamo odvažan novi dizajn kamere koji je kategorija za sebe. Odlikuje se ultra veličinom i konturno izrezanom kamerom koja u svojemu modulu neprimjetno skriva objektive najsuvremenije izrade.</p>
        </div>
        <div class="col-lg-6"><br><br>
            &nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;  <img src="images/s21ultra2.jpg" width="600px" height="400px" class="rounded">
        </div>
    </div>
    <div class="row bg-dark">
        <br><br><br>
        <div class="col-lg-6"><br><br>
            <br>
            <img src="images/s21ultra3.jpg" width="500  px" height="350px" class="rounded" style="margin-left: 30px;"> 
        </div>
        <div class="col-lg-6">
            <br>
            <br>
            <br>
            <br>
            <h2 style="color: white;"><b>Najviša moguća rezolucija na mobilnom telefonu</b></h2>
            <p style="color: white;">Njegova 108-megapikselna kamera snima s toliko mnogo detalja, da povlačenjem prstima možete približiti prikaz kako biste otkrili još mnogo novih kadrova unutar onoga prvotnoga. Kamera ovakve kvalitete stvara mnoštvo podataka o boji koji pomažu u stvaranju sadržaja prepuna detalja i nijansi poput stvarnih, bez efekta ispranosti zbog utjecaja sunčeve svjetlosti.</p> 
        </div>
    </div>
    <div class="row bg-dark">
        <div class="col-lg-6">
            <br>
            <br>
            <br>
            <br>
            <br>
            <h2 style="color: white; margin-left: 40px;"><b>Najčvršće staklo dosad</b></h2>
            <p style="color: white; margin-left: 40px;">Dizajnirano kako bi se manje grebalo i za zaštitu sprijeda i straga,    ovo je najtvrđe Gorilla Glass staklo na pametnom telefonu.</p>
        </div>
        <div class="col-lg-6"><br><br><br>
            &nbsp;&nbsp;&nbsp;    &nbsp;&nbsp;&nbsp; <img src="images/s21ultra4.jpg" width="550px" height="360px" class="rounded"><br><br>
        </div>
    </div>
    <div class="row bg-dark">
        <br><br><br>
        <div class="col-lg-6"><br><br>
            <br>
            <img src="images/s21ultra5.jpg" width="500  px" height="350px" class="rounded" style="margin-left: 30px;"> 
        </div>
        <div class="col-lg-6">
            <br>
            <br>
            <br>
            <br>
            <br>
            <h2 style="color: white;"><b>Prvi Galaxy 5nm procesor</b></h2>
            <p style="color: white;">Prvi Galaxy 5nm procesor ima epsku snagu i brzinu. Ova izvanredna nadogradnja znači brže učenje i više inteligencije u svim aspektima Galaxy S21 Ultra 5G.</p> 
        </div>
    </div>
</div>
<br><br>

<div class="container">
    <div class="col-lg-12">
    <h1>Tehničke specifikacije</h1>        
    <table class="table table-light table-hover">
      <tbody>
        <tr>
            <td>Podržane mreže</td>
            <td>GSM 850, LTE 1800, LTE 2600, 900, 1800, 1900, UMTS 850, UMTS 900, UMTS 1900, UMTS 2100, LTE 800 MHz</td>
          </tr>
        <tr>
          <td>Prijenos podataka</td>
          <td>GPRS, HSDPA 42.2, EDGE, LTE Cat20 2000 Mbps DL, 5G, UMTS, HSUPA, HSUPA 5.76</td>
        </tr>
        <tr>
          <td>Povezivost</td>
          <td>Bluetooth, NFC, Sinkronizacija s racunalom, USB kabel, WLAN, GPS</td>
        </tr>
        <tr>
            <td>Poruke</td>
            <td>SMS, MMS, E-mail klijent</td>
        </tr>
        <tr>
            <td>Zaslon</td>
            <td>Dynamic AMOLED 2X</td>
        </tr>
        <tr>
            <td>Kamera</td>
            <td>Četverostruka glavna (108 Mpx + 10 Mpx + 10 Mpx + 12 Mpx) + Selfie (40 Mpx)</td>
        </tr>
        <tr>
            <td>Memorija telefona</td>
            <td>Radna memorija 12GB, korisnička i sistemska memorija 256GB</td>
        </tr>
        <tr>
            <td>Procesor</td>
            <td>Exynos 2100 (5 nm)</td>
        </tr>
        <tr>
            <td>Dimenzije uređaja</td>
            <td>165.1 x 75.6 x 8.9 mm</td>
        </tr>
        <tr>
            <td>Težina</td>
            <td>221 g</td>
        </tr>
      </tbody>
    </table>
    </div>
</div>
<br>
  
<a class="back-to-top"><i class="fas fa-arrow-up"></i></a>

      <!--https://w3schoolweb.com/bootsrap5-footer-with-social-media-icons/-->
      <section class="footer">
        <div class="container">
          <div class="footer__content">
            <div class="footer__heading">
              <h2>MobilMania</h2>
            </div>
            <p class="mb-0">Copyright &copy; 2021 mato3089@gmail.com</p>
       
            <ul class="social__media">
              <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></li>
              <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
            </ul>
          </div>
        </div>
      </section>
      
</body>
</html> 