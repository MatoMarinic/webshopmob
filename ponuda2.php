<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>MobilMania WebShop</title>

  <!--BOOTSTRAP-->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"> </script>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

  <!---->
  <script src="https://kit.fontawesome.com/4a15fc06a1.js" crossorigin="anonymous"></script>

  <link href="style/ponudacss.css" rel="stylesheet">

  <script src="js/javascript.js"></script>
</head>

<body>
<?php include('navbar.php'); ?>
  <br><br><br>

  <!-- Page Content -->
  <div class="container">
    <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Ponuda mobitela</li>
      </ol>
    </nav>
  </div>

  <!--OVDJE DODATI GALERIJU SLIKA I BUTTON-->
  <div class="container-fluid">
    <div class="row row-cols-auto">
      <div class="col">
        <div class="card h-100 filtar">
          <h4 class="card-header">
            Filtriraj</h4>
          <div class="card-body">
            <p id="proizvodac_p"><b>Proizvođač</b></p>
            <form action="">
              <label for="proizvodac1" class="checkbox">
                <input type="checkbox" id="proizvodac1" name="proizvodac1">
                <span>Samsung</span></label>
              <br>
              <label for="proizvodac2" class="checkbox">
                <input type="checkbox" id="proizvodac2" name="proizvodac2">
                <span>iPhone</span>
              </label><br>

              <label for="proizvodac3" class="checkbox">
                <input type="checkbox" id="proizvodac3" name="proizvodac3">
                <span>Motorola</span>
              </label><br>

              <label for="proizvodac4" class="checkbox">
                <input type="checkbox" id="proizvodac4" name="proizvodac4"><span>Huawei</span></label><br>

              <label for="proizvodac5" class="checkbox">
                <input type="checkbox" id="proizvodac5" name="proizvodac5"><span>Xiaomi</span></label>

            </form>
            <hr>
            <p id="proizvodac"><b>Prikaži prvo</b></p>
            <form action="">

              <label for="najskuplji" class="radiobox">
                <input type="radio" id="najskuplji" name="filtar_mob"><span>Najskuplji</span></label><br>

              <label for="najjeftiniji" class="radiobox"> <input type="radio" id="najjeftiniji"
                  name="filtar_mob"><span>Najjeftiniji</span></label><br>

            </form>
            <hr>
          </div>
        </div>
      </div>

      <div class="col">
        <div class="card h-100 card_mob1">
          <h4 class="card-header">
            Samsung</h4>
          <p>&nbsp;<b>Samsung Galaxy Z Fold 3 256GB crni 5G</b></p>
          <div class="card-body">
            <a href="z3.html"><img src="images/z3.webp" width="350px" height="355px"></a>
            <p class="card-text" id="mob1"><br>
              &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;<b>12.500,00 kn</b><br></p>
            <hr>
            <p class="card-text" id="mob1">&nbsp; &nbsp;&nbsp;<b>Super cijena!</b> </p>
            <a href="z3.html"><button class="button-24" role="button" id="odaberi1">Odaberi <i class="fas fa-arrow-right"></i></button></a>
          </div>
        </div>
      </div>

      <div class="col">
        <div class="card h-100 card_mob1">
          <h4 class="card-header">
            Motorola</h4>
          <p>&nbsp;<b>Motorola Moto E20 crna</b></p>
          <div class="card-body">
            <a href="motoe20.html"><img src="images/motoe20.jpeg" width="352px" height="350px"></a>

            <p class="card-text" id="mob1"><br>
              &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;<b> 990.00,00 kn</b><br></p>
            <hr>
            <p class="card-text" id="mob1">&nbsp; &nbsp;&nbsp;<b>Super cijena!</b> </p>
            <a href="motoe20.html"><button class="button-24" role="button" id="odaberi1">Odaberi <i class="fas fa-arrow-right"></i></button></a>
          </div>
        </div>
      </div>
      <div class="col">
        <div class="card h-100 card_mob1">
          <h4 class="card-header">
            Huawei</h4>
          <p>&nbsp;<b>Huawei Mate Xs 512GB plavi</b></p>
          <div class="card-body">
            <a href="matexs.html"><img src="images/matexs.png" width="300px" height="350px"></a>

            <p class="card-text" id="mob1"><br>
              &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;<b>14.500,00 kn</b><br></p>
            <hr>
            <p class="card-text" id="mob1">&nbsp; &nbsp;&nbsp;<b>Super cijena!</b> </p>
            <a href="matexs.html"><button class="button-24" role="button" id="odaberi1">Odaberi <i class="fas fa-arrow-right"></i></button></a>
          </div>
        </div>
      </div>

    </div>
    <br>
  </div>
    <div class="container">
      <div class="d-flex justify-content-center">
        <a href="ponuda.html" class="previous">&laquo; Prethodna</a>
        <a href="#" class="next" style="pointer-events: none">Sljedeća &raquo;</a>
      </div>
    </div>
    <a class="back-to-top"><i class="fas fa-arrow-up"></i></a>

    <br><br>
    <!--https://w3schoolweb.com/bootsrap5-footer-with-social-media-icons/-->
    <section class="footer">
      <div class="container">
        <div class="footer__content">
          <div class="footer__heading">
            <h2>MobilMania</h2>
          </div>
          <p class="mb-0">Copyright &copy; 2021 mato3089@gmail.com</p>

          <ul class="social__media">
            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
            <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></li>
            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
            <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
          </ul>
        </div>
      </div>
    </section>
</body>

</html>