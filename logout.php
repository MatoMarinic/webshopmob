<?php
session_start();
unset($_SESSION["loggedin"]);
unset($_SESSION["email"]);
unset($_SESSION['role']);
unset($_SESSION['username']);
header("Location: index.php");
?>