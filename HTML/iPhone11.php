<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MobilMania WebShop</title>

    <!--BOOTSTRAP-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"> </script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!---->
    <script src="https://kit.fontawesome.com/4a15fc06a1.js" crossorigin="anonymous"></script>

    <link href="../style/css.css" rel="stylesheet">

    <script src="../js/javascript.js"></script>
</head>

<body>

<?php include('../view/navbar.php'); ?>
    <br>
    <br><br><br>


    <div class="container">
        <div class="row d-flex justify-content-center">
          <div class="col-lg-4 mb-4">
            <div class="card h-100">
              <h4 class="card-header">
            iPhone</h4>
            <p>&nbsp;<b>iPhone 11 64GB sivi 2020</b></p>
          <div class="card-body"> 
            &nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;  <img src="../images/iphone11.jpg" width="270px" height="350px">
               
            <p class="card-text" id="mob1"><br>
           &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;<b>4.464,00 kn</b><br></p>
           <hr>
           <p class="card-text" id="mob1">&nbsp; &nbsp;&nbsp;<b>Super cijena!</b> </p>
          </div>
        </div>
      </div>
      <div class="col-lg-4 justify-content-lg-center dugme">
      <form action="../controllers/CartController.php" method="post">
          <input type="hidden" name="name" value="iPhone 11 64GB sivi 2020">
          <input type="hidden" name="price" value="4464">
          <button class="button-24" type="submit" id="odaberi1">Dodaj u košaricu <i class="fas fa-arrow-right"></i></button>
        </form>
      </div>
    </div>
</div>

<div class="container-fluid">
    <br><br><br>
    <h1>&nbsp;&nbsp;&nbsp;O uređaju</h1>
    <br>
    <div class="row bg-dark">
        <div class="col-lg-6">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/iphone111.png" width="500px" height="350px" class="rounded"> 
        </div>
        <div class="col-lg-6">
            <br>
            <br>
            <br>
            <br>
            <br>
            <h2 style="color: white;"><b>Novi sustav dvostruke kamere</b></h2>
            <p style="color: white;">Snimaj 4K videozapise, predivne portrete i zadivljujuće krajolike s potpuno novim sustavom dvostruke kamere. Snimi najbolje fotografije čak i pri slaboj osvijetljenosti pomoću noćnog načina rada.</p> 
        </div>        
    </div>
    <div class="row bg-dark">
        <div class="col-lg-6">
            <br>
            <br>
            <br>
            &nbsp;&nbsp;&nbsp; <h2 style="color: white; margin-left: 40px;"><b>Noć u potpuno novom svjetlu</b></h2>
            <p style="color: white; margin-left: 40px;">Od rođendanskih svjećica do logorske vatre na plaži, novi Noćni način omogućuje da snimiš vjerne fotografije pri slaboj osvijetljenosti – automatski.
                Podigni videozapise na novu razinu uz 4K videozapise pri 60 fps na svakoj kameri i ultra široku kameru za četiri puta širi kadar. Zakreni, izreži i dodaj filtere jednako lako kao na fotografijama.</p>
        </div>
        <div class="col-lg-6"><br><br>
            &nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;  <img src="../images/iphone112.png" width="550px" height="400px" class="rounded">
        </div>
    </div>
    <div class="row bg-dark">
        <br><br><br>
        <div class="col-lg-6"><br><br>
            <br>
            <img src="../images/iphone113.png" width="450px" height="350px" class="rounded" style="margin-left: 30px;"> 
        </div>
        <div class="col-lg-6">
            <br>
            <br>
            <br>
            <h2 style="color: white;"><b>Doživi više</b></h2>
            <p style="color: white;">Doživi dosad nezabilježene performanse zahvaljujući čipu A13 Bionic za igranje videoigara, <br>proširenu stvarnost (AR) i fotografiranje. Ovo je najbrži čip za smartphone na svijetu.</p> 
        </div>
    </div>
    <div class="row bg-dark">
        <div class="col-lg-6">
            <br>
            <br>
            <br>
            <h2 style="color: white; margin-left: 40px;"><b>Odvedi svoje fotografije korak naprijed. I u širinu.</b></h2>
            <p style="color: white; margin-left: 40px;">Ultra široka (13 mm) ili široka kamera (26 mm), koju biraš? Vidno polje od 120° za četiri puta veći kadar. Stopostotni pikseli za fokusiranje za tri puta brže automatsko fokusiranje u uvjetima slabe osvijetljenosti.
                Ne zaboravi ni na selfie jer prednja kamera TrueDepth od 12 MP dolazi s portretnim načinom i Slo-Mo snimanjem.</p>
        </div>
        <div class="col-lg-6"><br><br><br>
            &nbsp;&nbsp;&nbsp;    &nbsp;&nbsp;&nbsp; <img src="../images/iphone114.jpg" width="500px" height="360px" class="rounded"><br><br>
        </div>
    </div>
</div>
<br><br>

<div class="container">
    <div class="col-lg-12">
    <h1>Tehničke specifikacije</h1>        
    <table class="table table-light table-hover">
      <tbody>
        <tr>
            <td>Podržane mreže</td>
            <td>GSM 850, LTE 1800, LTE 2600, 900, 1800, 1900, UMTS 850, UMTS 900, UMTS 1900, UMTS 2100, LTE 800 MHz</td>
          </tr>
        <tr>
          <td>Prijenos podataka</td>
          <td>GPRS, HSDPA 42.2, EDGE, LTE Cat21 1400 Mbps DL, HSUPA 5.76</td>
        </tr>
        <tr>
          <td>Povezivost</td>
          <td>Bluetooth, NFC, Sinkronizacija s racunalom, USB kabel, WLAN, GPS</td>
        </tr>
        <tr>
            <td>Poruke</td>
            <td>SMS, MMS, E-mail klijent</td>
        </tr>
        <tr>
            <td>Zaslon</td>
            <td>6.1" Liquid Retina IPS LCD dodirni zaslon razlučivosti 828 x 1792 piksela</td>
        </tr>
        <tr>
            <td>Kamera</td>
            <td>Dvostruka glavna (12 Mpx + 12 Mpx) + Selfie (12 Mpx)</td>
        </tr>
        <tr>
            <td>Memorija telefona</td>
            <td>Radna memorija 4GB, korisnička i sistemska memorija 64GB</td>
        </tr>
        <tr>
            <td>Procesor</td>
            <td>Apple A13 Bionic (7 nm+)</td>
        </tr>
        <tr>
            <td>Dimenzije uređaja</td>
            <td>150.9 x 75.7 x 8.3 mm</td>
        </tr>
        <tr>
            <td>Težina</td>
            <td>194 g</td>
        </tr>
      </tbody>
    </table>
    </div>
</div>
<br>
  
<a class="back-to-top"><i class="fas fa-arrow-up"></i></a>

      <!--https://w3schoolweb.com/bootsrap5-footer-with-social-media-icons/-->
      <section class="footer">
        <div class="container">
          <div class="footer__content">
            <div class="footer__heading">
              <h2>MobilMania</h2>
            </div>
            <p class="mb-0">Copyright &copy; 2021 mato3089@gmail.com</p>
       
            <ul class="social__media">
              <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></li>
              <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
            </ul>
          </div>
        </div>
      </section>
      
</body>
</html>