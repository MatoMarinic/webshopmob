<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MobilMania WebShop</title>

    <!--BOOTSTRAP-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"> </script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!---->
    <script src="https://kit.fontawesome.com/4a15fc06a1.js" crossorigin="anonymous"></script>

    <link href="../style/css.css" rel="stylesheet">

    <script src="../js/javascript.js"></script>
</head>

<body>

<?php include('../view/navbar.php'); ?>
    <br>
    <br><br><br>


    <div class="container">
        <div class="row d-flex justify-content-center">
          <div class="col-lg-4 mb-4">
            <div class="card h-100">
              <h4 class="card-header">
            Xiaomi</h4>
            <p>&nbsp;<b>Xiaomi Mi 11 lite 128GB 5G sivi</b></p>
          <div class="card-body"> 
            <img src="../images/xiamoi11.jpg" width="350px" height="350px">
               
            <p class="card-text" id="mob1"><br>
           &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;<b>1.999,00 kn</b><br></p>
           <hr>
           <p class="card-text" id="mob1">&nbsp; &nbsp;&nbsp;<b>Super cijena!</b> </p>
          </div>
        </div>
      </div>
      <div class="col-lg-4 justify-content-lg-center dugme">
      <form action="../controllers/CartController.php" method="post">
                <input type="hidden" name="name" value="Xiaomi Mi 11 lite 128GB 5G sivi">
                <input type="hidden" name="price" value="1999">
                <button class="button-24" type="submit" id="odaberi1">Dodaj u košaricu <i class="fas fa-arrow-right"></i></button>
        </form>
      </div>
    </div>
</div>

<div class="container-fluid">
    <br><br><br>
    <h1>&nbsp;&nbsp;&nbsp;O uređaju</h1>
    <br>
    <div class="row bg-dark">
        <br><br><br>
        <div class="col-lg-6">
            <br>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/xiaomi115g.jpg" width="500px" height="350px" class="rounded"> 
            <br>
        </div>
        <div class="col-lg-6">
            <br>
            <br>
            <br>
            <br>
            <h2 style="color: white;"><b>5G brzina</b></h2>
            <p style="color: white;">S pravim uređajem, 5G brzina donijet će više uzbuđenja i olakšati ti život i <br>pokazati tvoj stil u bilo kojem aspektu.</p> 
        </div>        
    </div>
    <div class="row bg-dark">
        <div class="col-lg-6">
            <br>
            <br>
            &nbsp;&nbsp;&nbsp; <h2 style="color: white; margin-left: 40px;"><b>Izuzetno tanak dizajn</b></h2>
            <p style="color: white; margin-left: 40px;">Simetrični i super tanki bočni i gornji okviri širine svega 1,88 mm, daju više prostora zaslonu, a izbočenje kamere od samo 1,7 mm savršeno se uklapaju u iznimno tanak dizajn.</p>
        </div>
        <div class="col-lg-6">
            <img src="../images/xiaomi2.jpg" width="650px" height="400px" class="rounded">
        </div>
    </div>
    <div class="row bg-dark">
        <br><br><br>
        <div class="col-lg-6"><br><br>
            <br>
            <img src="../images/xiaomi3.jpg" width="500px" height="350px" class="rounded" style="margin-left: 30px;"> 
        </div>
        <div class="col-lg-6">
            <br>
            <br>
            <br>
            <h2 style="color: white;"><b>Očaravajući izgled, uvijek kao nov</b></h2>
            <p style="color: white;">Uživaj u gledanju video zapisa i igranju igara na jasnom 6.55" AMOLED zaslonu, a uz poboljšanu prevenciju otisaka prstiju, tvoj će telefon uvijek izgledati kao da je potpuno nov.</p> 
        </div>
    </div>
    <div class="row bg-dark">
        <div class="col-lg-6">
            <br>
            <br>
            <br>
            <h2 style="color: white; margin-left: 20px;"><b>Sve je u detaljima</b></h2>
            <p style="color: white; margin-left: 20px;">FHD+ DotDisplay jasno kontrastira svjetlo i sjenu radi dobivanja vjernije slike. Oživi sadržaj bogatim bojama i velikom preciznošću boja.</p>
        </div>
        <div class="col-lg-6"><br><br>
            &nbsp;&nbsp;&nbsp;  <img src="../images/xiaomi4.jpg" width="600px" height="360px" class="rounded"><br><br>
        </div>
    </div>
</div>
<br><br>

<div class="container">
    <div class="col-lg-12">
    <h1>Tehničke specifikacije</h1>        
    <table class="table table-light table-hover">
      <tbody>
        <tr>
            <td>Podržane mreže</td>
            <td>GSM 850, LTE 1800, LTE 2600, 900, 1800, 1900, UMTS 850, UMTS 900, UMTS 1900, UMTS 2100, LTE 800 MHz</td>
          </tr>
        <tr>
          <td>Prijenos podataka</td>
          <td>GPRS, HSDPA 42.2, EDGE, LTE Cat4 150 Mbps DL, HSUPA, HSUPA 5.76</td>
        </tr>
        <tr>
          <td>Povezivost</td>
          <td>Bluetooth, USB kabel, WLAN, GPS</td>
        </tr>
        <tr>
            <td>Poruke</td>
            <td>SMS, MMS, E-mail klijent</td>
        </tr>
        <tr>
            <td>Zaslon</td>
            <td>AMOLED 6.55" + 1080 x 2400 piksela</td>
        </tr>
        <tr>
            <td>Kamera</td>
            <td>Trostruka glavna (64 Mpx + 8 Mpx + 5 Mpx) + Selfie (20 Mpx)</td>
        </tr>
        <tr>
            <td>Memorija telefona</td>
            <td>Radna memorija 6GB, korisnička i sistemska memorija 128GB</td>
        </tr>
        <tr>
            <td>Procesor</td>
            <td>Qualcomm SM7350-AB Snapdragon 780G (5 nm)</td>
        </tr>
        <tr>
            <td>Dimenzije uređaja</td>
            <td>160.5 x 75.7 x 6.8 mm</td>
        </tr>
        <tr>
            <td>Težina</td>
            <td>159 g</td>
        </tr>
      </tbody>
    </table>
    </div>
</div>
<br>
  
<a class="back-to-top"><i class="fas fa-arrow-up"></i></a>

      <!--https://w3schoolweb.com/bootsrap5-footer-with-social-media-icons/-->
      <section class="footer">
        <div class="container">
          <div class="footer__content">
            <div class="footer__heading">
              <h2>MobilMania</h2>
            </div>
            <p class="mb-0">Copyright &copy; 2021 mato3089@gmail.com</p>
       
            <ul class="social__media">
              <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></li>
              <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
            </ul>
          </div>
        </div>
      </section>
      
</body>
</html>