<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MobilMania WebShop</title>

    <!--BOOTSTRAP-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"> </script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!---->
    <script src="https://kit.fontawesome.com/4a15fc06a1.js" crossorigin="anonymous"></script>

    <link href="../style/css.css" rel="stylesheet">

    <script src="../js/javascript.js"></script>
</head>

<body>

<?php include('../navbar.php'); ?>
<br><br><br>
<div class="container">
    <div class="row">
        <h2>O uređaju</h2>
        <br><br><br>
        <div class="col-lg-6">
            
            <img src="../images/samsungs21narudzba.jpg" width="500px" height="350px" class="rounded"> 
        </div>
        <div class="col-lg-6">
            <h2>Za spektakularnu svakodnevicu</h2>
            <p>Upoznaj Galaxy S21 5G i S21+ 5G. Dizajniran da unese revoluciju u snimanje fotografija i videozapisa uz 8K rezoluciju koja nadilazi doživljaj gledanja filmova, tako da iz snimljenih videozapisa možeš dobiti spektakularne fotografije. Stvari su upravo postale spektakularne.</p> 
        </div>

    </div>
    <br><br><br>
    <div class="row">
        <div class="col-lg-6">
            <h2>Zabilježi spektakularne fotografije u svakom 8K videozapisu</h2>
            <p>8K videozapis najviša je rezolucija na mobilnom telefonu dosad, što znači da će tvoje snimke izgledati i bolje od filma. A uz 8K videozapise s 24 sličice u sekundi, svaki vlog i uspomena postaju tvoj sljedeći korak do zvijezda</p>
        </div>
        <div class="col-lg-6">
            <img src="../images/s21kamera.jpg" width="650px" height="350px" class="rounded">
        </div>
    </div>
    <br><br><br>
    <div class="row">
        <div class="col-lg-6">
            <img src="../images/s21ljepota.jpg" width="550px" height="350px" class="rounded">
        </div>
        <div class="col-lg-6">
            <h2>Ljepota je u detaljima</h2>
            <p>Snimaj u rezoluciji od 64 megapiksela kako bi mogao uhvatiti i zabilježiti detalje s jasnoćom koja će od tvojih fotografija napraviti umjetnička djela.</p>
        </div>
    </div>
    <br><br><br>
    <div class="row">
        <div class="col-lg-6">
            <h2>Upoznaj najbrži čip dosad u nekom Galaxy uređaju</h2>
            <p>Sa više brzine, više snage i više inteligencije, 5nm procesor je dovoljno brz da ide u korak sa akcijom, tako da možeš učiniti više uz veću brzinu.</p>
        </div>
        <div class="col-lg-6">
            <img src="../images/s21procesor.jpg" width="550px" height="350px" class="rounded">
        </div>
    </div>



</div>

<div class="container">
    <div class="col-lg-12">
    <h1>Tehničke specifikacije</h1>        
    <table class="table table-light table-hover">
      <tbody>
        <tr>
          <td>OS</td>
          <td>Android 12</td>
        </tr>
        <tr>
          <td>Dimenzije</td>
          <td>151,7 x 71,2 x 7.9 mm</td>
        </tr>
        <tr>
            <td>Težina</td>
            <td>169 g</td>
        </tr>
        <tr>
            <td>Zaslon</td>
            <td>6.2'' Dynamic AMOLED 2X, FHD+ (1080 x 2400)</td>
        </tr>
        <tr>
            <td>Memorija(RAM/ROM)</td>
            <td>RAM: 8 GB, ROM: 128 GB</td>
        </tr>
        <tr>
            <td>Memorijska kartica</td>
            <td>Ne</td>
        </tr>
        <tr>
            <td>Povezivost</td>
            <td>2G, 3G, 4G, 5G WIFI, USB C, bluetooth 5.2, otisak prsta (u zaslonu)</td>
        </tr>
        <tr>
            <td>Kamera (Glavna/Prednja)</td>
            <td>Glavna: 12 MP + 12 MP + 64 MP/ Prednja: 10 MP</td>
        <tr>
            <td>Procesor</td>
            <td>Exynos 2100, 1x2.9 GHz + 3x2.80 GHz + 4x2.2 GHz</td>
        </tr>
        <tr>
            <td>Baterija</td>
            <td>4000 mAh, vrijeme razgovora (h): do 33</td>
        </tr>
        <tr>
            <td>Sadržaj pakiranja</td>
            <td>Uređaj, data kabel, igla za izbacivanje SIM utora, kratke upute</td>
        </tr>
      </tbody>
    </table>
    </div>
</div>
  
<a class="back-to-top"><i class="fas fa-arrow-up"></i></a>

      <!--https://w3schoolweb.com/bootsrap5-footer-with-social-media-icons/-->
      <section class="footer">
        <div class="container">
          <div class="footer__content">
            <div class="footer__heading">
              <h2>MobilMania</h2>
            </div>
            <p class="mb-0">Copyright &copy; 2021 mato3089@gmail.com</p>
       
            <ul class="social__media">
              <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></li>
              <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
            </ul>
          </div>
        </div>
      </section>
      
</body>
</html>