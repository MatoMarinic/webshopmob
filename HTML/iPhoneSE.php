<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MobilMania WebShop</title>

    <!--BOOTSTRAP-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"> </script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!---->
    <script src="https://kit.fontawesome.com/4a15fc06a1.js" crossorigin="anonymous"></script>

    <link href="../style/css.css" rel="stylesheet">

    <script src="../js/javascript.js"></script>
</head>

<body>

<?php include('../view/navbar.php'); ?>
    <br>
    <br><br><br>


    <div class="container">
        <div class="row d-flex justify-content-center">
          <div class="col-lg-4 mb-4">
            <div class="card h-100">
              <h4 class="card-header">
            iPhone</h4>
            <p>&nbsp;<b>iPhone SE 64GB 2020 crni</b></p>
          <div class="card-body"> 
            &nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp; <img src="../images/iphonese.jpg" width="350px" height="350px">
               
            <p class="card-text" id="mob1"><br>
           &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;<b>2.999,00 kn</b><br></p>
           <hr>
           <p class="card-text" id="mob1">&nbsp; &nbsp;&nbsp;<b>Super cijena!</b> </p>
          </div>
        </div>
      </div>
      <div class="col-lg-4 justify-content-lg-center dugme">
      <form action="../controllers/CartController.php" method="post">
          <input type="hidden" name="name" value="iPhone SE 64GB 2020 crni">
          <input type="hidden" name="price" value="2999">
          <button class="button-24" type="submit" id="odaberi1">Dodaj u košaricu <i class="fas fa-arrow-right"></i></button>
        </form>
      </div>
    </div>
</div>

<div class="container-fluid">
    <br><br><br>
    <h1>&nbsp;&nbsp;&nbsp;O uređaju</h1>
    <br>
    <div class="row bg-dark">
        <div class="col-lg-6">
            <br>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/iphonese1.jpg" width="500px" height="350px" class="rounded"> 
        </div>
        <div class="col-lg-6">
            <br>
            <br>
            <br>
            <br>
            <br>
            <h2 style="color: white;"><b>Briljantan zaslon</b></h2>
            <p style="color: white;">Izdržljivo staklo i aluminijski dizajn. Briljantni 4,7'' Retina HD zaslon u iPhoneu SE.</p> 
        </div>        
    </div>
    <div class="row bg-dark">
        <div class="col-lg-6">
            <br>
            <br>
            <br>
            &nbsp;&nbsp;&nbsp; <h2 style="color: white; margin-left: 40px;"><b>Najbrži čip u smartphoneu</b></h2>
            <p style="color: white; margin-left: 40px;">Mozak iPhonea 11 Pro postavljen je u tijelo iPhonea SE. Da, iPhone SE dolazi s A13 Bionic čipom. Najbržim čipom ikada u svijetu smartphonea.</p>
        </div>
        <div class="col-lg-6"><br><br>
            &nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;  <img src="../images/iphonese2.jpg" width="600px" height="400px" class="rounded">
        </div>
    </div>
    <div class="row bg-dark">
        <br><br><br>
        <div class="col-lg-6"><br><br>
            <br>
            <img src="../images/iphonese3.jpg" width="500  px" height="350px" class="rounded" style="margin-left: 30px;"> 
        </div>
        <div class="col-lg-6">
            <br>
            <br>
            <br>
            <br>
            <h2 style="color: white;"><b>Visoka kvaliteta videa</b></h2>
            <p style="color: white;">4K video znači da ćeš vidjeti četiri puta više detalja nego u 1080p HD videu. I prošireni dinamički raspon dodaje veće detalje u svjetlu i sjenu.</p> 
        </div>
    </div>
    <div class="row bg-dark">
        <div class="col-lg-6">
            <br>
            <br>
            <br>
            <h2 style="color: white; margin-left: 40px;"><b>Neka bude svjetlo. I sjena.</b></h2>
            <p style="color: white; margin-left: 40px;">Smart HDR prepoznaje lice i inteligentno prikazuje prirodnije obrise i tonove kože. Također, u pozadini ističe osvjetljenja i sjene. Tako da svaki dio fotografije izgleda nevjerojatno.</p>
        </div>
        <div class="col-lg-6"><br><br><br>
            &nbsp;&nbsp;&nbsp;    &nbsp;&nbsp;&nbsp; <img src="../images/iphonese4.jpg" width="550px" height="360px" class="rounded"><br><br>
        </div>
    </div>
</div>
<br><br>

<div class="container">
    <div class="col-lg-12">
    <h1>Tehničke specifikacije</h1>        
    <table class="table table-light table-hover">
      <tbody>
        <tr>
            <td>Podržane mreže</td>
            <td>GSM 850, LTE 1800, LTE 2600, 900, 1800, 1900, UMTS 850, UMTS 900, UMTS 1900, UMTS 2100, LTE 800 MHz</td>
          </tr>
        <tr>
          <td>Prijenos podataka</td>
          <td>GPRS, HSDPA 42.2, EDGE, LTE Cat21 1400 Mbps DL, HSUPA 5.76</td>
        </tr>
        <tr>
          <td>Povezivost</td>
          <td>Bluetooth, NFC, Sinkronizacija s racunalom, USB kabel, WLAN, GPS</td>
        </tr>
        <tr>
            <td>Poruke</td>
            <td>SMS, MMS, E-mail klijent</td>
        </tr>
        <tr>
            <td>Zaslon</td>
            <td>4.7" IPS LCD Retina dodirni zaslon razlučivosti 750 x 1334 piksela</td>
        </tr>
        <tr>
            <td>Kamera</td>
            <td>12 Mpx</td>
        </tr>
        <tr>
            <td>Memorija telefona</td>
            <td>Radna memorija 3GB, korisnička i sistemska memorija 64GB</td>
        </tr>
        <tr>
            <td>Procesor</td>
            <td>Apple A13 Bionic (7 nm+)</td>
        </tr>
        <tr>
            <td>Dimenzije uređaja</td>
            <td>138.4 x 67.3 x 7.3 mm</td>
        </tr>
        <tr>
            <td>Težina</td>
            <td>148 g</td>
        </tr>
      </tbody>
    </table>
    </div>
</div>
<br>
  
<a class="back-to-top"><i class="fas fa-arrow-up"></i></a>

      <!--https://w3schoolweb.com/bootsrap5-footer-with-social-media-icons/-->
      <section class="footer">
        <div class="container">
          <div class="footer__content">
            <div class="footer__heading">
              <h2>MobilMania</h2>
            </div>
            <p class="mb-0">Copyright &copy; 2021 mato3089@gmail.com</p>
       
            <ul class="social__media">
              <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></li>
              <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
            </ul>
          </div>
        </div>
      </section>
      
</body>
</html> 