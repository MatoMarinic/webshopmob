<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MobilMania WebShop</title>

    <!--BOOTSTRAP-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"> </script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!---->
    <script src="https://kit.fontawesome.com/4a15fc06a1.js" crossorigin="anonymous"></script>

    <link href="../style/css.css" rel="stylesheet">

    <script src="../js/javascript.js"></script>
</head>

<body>

<?php include('../view/navbar.php'); ?>
    <br>
    <br><br><br>


    <div class="container">
        <div class="row d-flex justify-content-center">
          <div class="col-lg-4 mb-4">
            <div class="card h-100">
              <h4 class="card-header">
            Huwaei</h4>
            <p>&nbsp;<b>Huawei Nova 8i plavi</b></p>
          <div class="card-body"> 
            &nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp; <img src="../images/nova8i.jpg" width="350px" height="350px">
               
            <p class="card-text" id="mob1"><br>
           &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;<b>2.699,00 kn</b><br></p>
           <hr>
           <p class="card-text" id="mob1">&nbsp; &nbsp;&nbsp;<b>Super cijena!</b> </p>
          </div>
        </div>
      </div>
      <div class="col-lg-4 justify-content-lg-center dugme">
      <form action="../controllers/CartController.php" method="post">
                <input type="hidden" name="name" value="Huawei Nova 8i plavi">
                <input type="hidden" name="price" value="2699">
                <button class="button-24" type="submit" id="odaberi1">Dodaj u košaricu <i class="fas fa-arrow-right"></i></button>
        </form>
      </div>
    </div>
</div>

<div class="container-fluid">
    <br><br><br>
    <h1>&nbsp;&nbsp;&nbsp;O uređaju</h1>
    <br>
    <div class="row bg-dark">
        <div class="col-lg-6">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/nova8i1.png" width="500px" height="350px" class="rounded"> 
        </div>
        <div class="col-lg-6">
            <br>
            <br>
            <br>
            <br>
            <br>
            <h2 style="color: white;"><b>Ljepota u vašoj ruci</b></h2>
            <p style="color: white;">Nasljeđujući svu vrhunsku estetiku i dinamičke boje nova serije, HUAWEI nova 8i donosi vam izvanredan vizualni doživljaj i vrhunski osjećaj prilikom korištenja.</p> 
        </div>        
    </div>
    <div class="row bg-dark">
        <div class="col-lg-6">
            <br>
            <br>
            <br>
            &nbsp;&nbsp;&nbsp; <h2 style="color: white; margin-left: 40px;"><b>Bezgranično gledanje, neograničena zabava</b></h2>
            <p style="color: white; margin-left: 40px;">Omjer zaslona u odnosu na kućište na ovom HUAWEI zaslonu bez rubova od 6,67 inča dolazi s <br>izvanrednih 94,7%, za iznimno uski okvir.</p>
        </div>
        <div class="col-lg-6"><br><br>
            &nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;  <img src="../images/nova8i2.png" width="550px" height="400px" class="rounded">
        </div>
    </div>
    <div class="row bg-dark">
        <br><br><br>
        <div class="col-lg-6"><br><br>
            <br>
            <img src="../images/nova8i3.png" width="550px" height="350px" class="rounded" style="margin-left: 30px;"> 
        </div>
        <div class="col-lg-6">
            <br>
            <br>
            <br>
            <h2 style="color: white;"><b>Snažan, spreman za rad</b></h2>
            <p style="color: white;">Podržavajući 66 W HUAWEI SuperCharge tehnologiju punjenja, <br>telefon se može napuniti do 60% u 17 minuta i potpuno napuniti u 38 minuta.</p> 
        </div>
    </div>
    <div class="row bg-dark">
        <div class="col-lg-6">
            <br>
            <br>
            <br>
            <h2 style="color: white; margin-left: 40px;"><b>Kamera visoke rezolucije</b></h2>
            <p style="color: white; margin-left: 40px;">Zahvaljujući velikom, naprednom senzoru, glavna kamera od 64 MP pomaže vam snimiti sve najbolje trenutke sa živopisnim detaljima.</p>
        </div>
        <div class="col-lg-6"><br><br><br>
            &nbsp;&nbsp;&nbsp;    &nbsp;&nbsp;&nbsp; <img src="../images/nova8i4.png" width="600px" height="360px" class="rounded"><br><br>
        </div>
    </div>
</div>
<br><br>

<div class="container">
    <div class="col-lg-12">
    <h1>Tehničke specifikacije</h1>        
    <table class="table table-light table-hover">
      <tbody>
        <tr>
            <td>Podržane mreže</td>
            <td>GSM 850, LTE 1800, LTE 2600, 900, 1800, 1900, UMTS 850, UMTS 900, UMTS 1900, UMTS 2100, LTE 800 MHz</td>
          </tr>
        <tr>
          <td>Prijenos podataka</td>
          <td>GPRS, HSDPA 42.2, EDGE, LTE Cat21 1400 Mbps DL, HSUPA 5.76</td>
        </tr>
        <tr>
          <td>Povezivost</td>
          <td>Bluetooth, USB kabel, WLAN, GPS</td>
        </tr>
        <tr>
            <td>Poruke</td>
            <td>SMS, MMS, E-mail klijent</td>
        </tr>
        <tr>
            <td>Zaslon</td>
            <td>6.67-inčni TFT LCD</td>
        </tr>
        <tr>
            <td>Kamera</td>
            <td>Četverostruka glavna (64 Mpx + 8 Mpx + 2 Mpx + 2 Mpx) + Selfie (20 Mpx)</td>
        </tr>
        <tr>
            <td>Memorija telefona</td>
            <td>Radna memorija 6GB, korisnička i sistemska memorija 128GB</td>
        </tr>
        <tr>
            <td>Procesor</td>
            <td>Qualcomm SM6115 Snapdragon 662 (11 nm)</td>
        </tr>
        <tr>
            <td>Dimenzije uređaja</td>
            <td>161.85 x 74.7 x 8.58 mm</td>
        </tr>
        <tr>
            <td>Težina</td>
            <td>194 g</td>
        </tr>
      </tbody>
    </table>
    </div>
</div>
<br>
  
<a class="back-to-top"><i class="fas fa-arrow-up"></i></a>

      <!--https://w3schoolweb.com/bootsrap5-footer-with-social-media-icons/-->
      <section class="footer">
        <div class="container">
          <div class="footer__content">
            <div class="footer__heading">
              <h2>MobilMania</h2>
            </div>
            <p class="mb-0">Copyright &copy; 2021 mato3089@gmail.com</p>
       
            <ul class="social__media">
              <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></li>
              <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
            </ul>
          </div>
        </div>
      </section>
      
</body>
</html> 