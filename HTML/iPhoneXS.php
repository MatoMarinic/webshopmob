<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MobilMania WebShop</title>

    <!--BOOTSTRAP-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"> </script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!---->
    <script src="https://kit.fontawesome.com/4a15fc06a1.js" crossorigin="anonymous"></script>

    <link href="../style/css.css" rel="stylesheet">

    <script src="../js/javascript.js"></script>
</head>

<body>

<?php include('../view/navbar.php'); ?>
    <br>
    <br><br><br>


    <div class="container">
        <div class="row d-flex justify-content-center">
          <div class="col-lg-4 mb-4">
            <div class="card h-100">
              <h4 class="card-header">
            iPhone</h4>
            <p>&nbsp;<b> ZOOT iPhone XS sivi</b></p>
          <div class="card-body"> 
            <img src="../images/xsiph.png" width="350px" height="350px">
               
            <p class="card-text" id="mob1"><br>
           &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;<b>9.351 kn</b><br></p>
           <hr>
           <p class="card-text" id="mob1">&nbsp; &nbsp;&nbsp;<b>Super cijena!</b> </p>
          </div>
        </div>
      </div>
      <div class="col-lg-4 justify-content-lg-center dugme" >
      <form action="../controllers/CartController.php" method="post">
          <input type="hidden" name="name" value="ZOOT iPhone XS sivi">
          <input type="hidden" name="price" value="9351">
          <button class="button-24" type="submit" id="odaberi1">Dodaj u košaricu <i class="fas fa-arrow-right"></i></button>
        </form>
      </div>
    </div>
</div>

<br>
<div class="container">
    <br><br><br>
    <h1>O uređaju</h1>
    <br>
    <div class="row">
        <div class="col-lg-6">
            <h2>Obnovljeni Zoot mobiteli</h2>
            <p>Obnovljeni Zoot iPhone mobiteli su ranije korišteni mobiteli koji ulaze u najvišu kategoriju očuvanosti i funkcionalnosti A+ što znači da izgledaju i rade kao novi, a dostupni su ti po nižoj cijeni!
                Na njima su provjerene sve funkcionalnosti, ispunjavaju najviše standarde te je njihova ispravnost detaljno testirana.Više o obnovljenim mobitelima pročitaj <a href="https://www.a1.hr/privatni/mobiteli/obnovljeni-mobiteli" target="_blank">ovdje.</a></p>
        </div>
        <div class="col-lg-6">
            <img src="../images/zootxs.jpg" width="650px" height="350px" class="rounded">
        </div>
    </div>
</div>

<div class="container">
    <div class="col-lg-12">
    <h1>Tehničke specifikacije</h1>        
    <table class="table table-light table-hover">
      <tbody>
        <tr>
          <td>Podržane mreže</td>
          <td>GSM 850, 900, 1800, 1900, UMTS 850, UMTS 900, UMTS 1900, UMTS 2100 MHz</td>
        </tr>
        <tr>
          <td>Dimenzije uređaja	</td>
          <td>1125 x 2436 mm</td>
        </tr>
        <tr>
            <td>Pakiranje</td>
            <td>USB kabel</td>
        </tr>
      </tbody>
    </table>
    </div>
</div>
  
<a class="back-to-top"><i class="fas fa-arrow-up"></i></a>

      <!--https://w3schoolweb.com/bootsrap5-footer-with-social-media-icons/-->
      <section class="footer">
        <div class="container">
          <div class="footer__content">
            <div class="footer__heading">
              <h2>MobilMania</h2>
            </div>
            <p class="mb-0">Copyright &copy; 2021 mato3089@gmail.com</p>
       
            <ul class="social__media">
              <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></li>
              <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
            </ul>
          </div>
        </div>
      </section>
      
</body>
</html>