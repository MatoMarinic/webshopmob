<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MobilMania WebShop</title>

    <!--BOOTSTRAP-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"> </script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!---->
    <script src="https://kit.fontawesome.com/4a15fc06a1.js" crossorigin="anonymous"></script>

    <link href="../style/css.css" rel="stylesheet">

    <script src="../js/javascript.js"></script>
</head>

<body>

<?php include('../view/navbar.php'); ?>
    <br>
    <br><br><br>


    <div class="container">
        <div class="row d-flex justify-content-center">
          <div class="col-lg-4 mb-4">
            <div class="card h-100">
              <h4 class="card-header">
            Samsung</h4>
            <p>&nbsp;<b>Samsung Galaxy S10+ 1TB DS crni</b></p>
          <div class="card-body"> 
            &nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;  <img src="../images/s10plus.jpg" width="270px" height="350px">
               
            <p class="card-text" id="mob1"><br>
           &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;<b>3.789,00 kn</b><br></p>
           <hr>
           <p class="card-text" id="mob1">&nbsp; &nbsp;&nbsp;<b>Super cijena!</b> </p>
          </div>
        </div>
      </div>
      <div class="col-lg-4 justify-content-lg-center dugme">
      <form action="../controllers/CartController.php" method="post">
                <input type="hidden" name="name" value="Samsung Galaxy S10+ 1TB DS crni">
                <input type="hidden" name="price" value="3789">
                <button class="button-24" type="submit" id="odaberi1">Dodaj u košaricu <i class="fas fa-arrow-right"></i></button>
        </form>
      </div>
    </div>
</div>

<div class="container-fluid">
    <br><br><br>
    <h1>&nbsp;&nbsp;&nbsp;O uređaju</h1>
    <br>
    <div class="row bg-dark">
        <br><br><br>
        <div class="col-lg-6">
            <br>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/s10plus1.png" width="500px" height="350px" class="rounded"> 
            <br>
        </div>
        <div class="col-lg-6">
            <br>
            <br>
            <br>
            <br>
            <h2 style="color: white;"><b>Nova generacija u novom ruhu</b></h2>
            <p style="color: white;">Odaberi jedan od tri vrhunska modela te uživaj u besprijekornom dizajnu i naprednoj inteligenciji.<br> Bez udubljenja, bez smetnji. Precizni laserski rez, zaštita na zaslonu i Dynamic AMOLED zaslon koji štiti oči <br>tako da je Infinity-O zaslon najinovativniji Galaxy zaslon do sad</p> 
        </div>        
    </div>
    <div class="row bg-dark">
        <div class="col-lg-6">
            <br>
            <br>
            <br>
            &nbsp;&nbsp;&nbsp; <h2 style="color: white; margin-left: 40px;"><b>Profesionalna kamera koja te pretvara u profesionalca</b></h2>
            <p style="color: white; margin-left: 40px;">Inteligentna kamera pomaže ti zabilježiti zapanjujuće fotografije tako što predlaže kompoziciju i <br>automatski optimizira postavke kamere prema sceni. Koristi iznimno širokokutnu kameru za zapanjujuće kinematografske fotografije s vidnim poljem od 123 stupnjeva.</p>
        </div>
        <div class="col-lg-6"><br><br>
            &nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;  <img src="../images/s10plus3.png" width="500px" height="400px" class="rounded">
        </div>
    </div>
    <div class="row bg-dark">
        <br><br><br>
        <div class="col-lg-6"><br><br>
            <br>
            <img src="../images/s10plus2.png" width="500px" height="350px" class="rounded" style="margin-left: 30px;"> 
        </div>
        <div class="col-lg-6">
            <br>
            <br>
            <br>
            <h2 style="color: white;"><b>Uvijek korak ispred</b></h2>
            <p style="color: white;">Inteligencija i napredni hardver uče kako koristite telefon te ga prilagođavaju kako bi dulje trajao i radio učinkovitije. Izradili smo i hardver i softver koji ti omogućuju bolje igranje bez zastoja.</p> 
        </div>
    </div>
    <div class="row bg-dark">
        <div class="col-lg-6">
            <br>
            <br>
            <br>
            <h2 style="color: white; margin-left: 40px;"><b>Moć dijeljenja</b></h2>
            <p style="color: white; margin-left: 40px;">Bežično punjenje drugih uređaja omogućuje ti punjenje baterije drugih uređaja. <br>A uz brzo bežično punjenje 2.0 još brže napuni bateriju na 100 %.</p>
        </div>
        <div class="col-lg-6"><br><br><br>
            &nbsp;&nbsp;&nbsp;  <img src="../images/s10plus4.png" width="600px" height="360px" class="rounded"><br><br>
        </div>
    </div>
</div>
<br><br>

<div class="container">
    <div class="col-lg-12">
    <h1>Tehničke specifikacije</h1>        
    <table class="table table-light table-hover">
      <tbody>
        <tr>
            <td>Podržane mreže</td>
            <td>GSM 850, LTE 1800, LTE 2600, 900, 1800, 1900, UMTS 850, UMTS 900, UMTS 1900, UMTS 2100, LTE 800 MHz</td>
          </tr>
        <tr>
          <td>Prijenos podataka</td>
          <td>GPRS, HSDPA 42.2, LTE Cat20 2000 Mbps DL, UMTS, HSUPA 5.76</td>
        </tr>
        <tr>
          <td>Povezivost</td>
          <td>Bluetooth, NFC, Sinkronizacija s racunalom, USB kabel, WLAN, GPS</td>
        </tr>
        <tr>
            <td>Poruke</td>
            <td>SMS, MMS, E-mail klijent</td>
        </tr>
        <tr>
            <td>Zaslon</td>
            <td>6.4" Dynamic AMOLED dodrirni zaslon razlučivosti 1440 x 3040 piksela</td>
        </tr>
        <tr>
            <td>Kamera</td>
            <td>Trostruka glavna (12 Mpx + 12 Mpx + 16 Mpx) + Dvostruka Selfie (10 Mpx + 12 Mpx)</td>
        </tr>
        <tr>
            <td>Memorija telefona</td>
            <td>Radna memorija 12GB, korisnička i sistemska memorija 1TB</td>
        </tr>
        <tr>
            <td>Procesor</td>
            <td>Exynos 9820 Octa (8 nm) - EMEA</td>
        </tr>
        <tr>
            <td>Dimenzije uređaja</td>
            <td>157.6 x 74.1 x 7.8 mm</td>
        </tr>
        <tr>
            <td>Težina</td>
            <td>175 g</td>
        </tr>
      </tbody>
    </table>
    </div>
</div>
<br>
  
<a class="back-to-top"><i class="fas fa-arrow-up"></i></a>

      <!--https://w3schoolweb.com/bootsrap5-footer-with-social-media-icons/-->
      <section class="footer">
        <div class="container">
          <div class="footer__content">
            <div class="footer__heading">
              <h2>MobilMania</h2>
            </div>
            <p class="mb-0">Copyright &copy; 2021 mato3089@gmail.com</p>
       
            <ul class="social__media">
              <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></li>
              <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
            </ul>
          </div>
        </div>
      </section>
      
</body>
</html>