<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MobilMania WebShop</title>

    <!--BOOTSTRAP-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"> </script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!---->
    <script src="https://kit.fontawesome.com/4a15fc06a1.js" crossorigin="anonymous"></script>

    <link href="../style/css.css" rel="stylesheet">

    <script src="../js/javascript.js">

    </script>


<body>

<?php include('../view/navbar.php'); ?>
    <br>
    <br><br><br>


    <div class="container">
        <div class="row row-cols-auto d-flex justify-content-center">
            <div class="col">
                <div class="card h-100">
                    <h4 class="card-header">
                        Samsung</h4>
                    <p>&nbsp;<b>Samsung Galaxy Z Fold 3 256GB crni 5G</b></p>
                    <div class="card-body">
                        &nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp; <img src="../images/motoe20.jpeg" width="355px"
                            height="350px">

                        <p class="card-text" id="mob1"><br>
                            &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;<b>990.00,00 kn</b><br></p>
                        <hr>
                        <p class="card-text" id="mob1">&nbsp; &nbsp;&nbsp;<b>Super cijena!</b> </p>
                    </div>  
                </div>
            </div>
            <div class="col justify-content-lg-center dugme">
            <form action="../controllers/CartController.php" method="post">
                <input type="hidden" name="name" value="Samsung Galaxy Z Fold 3 256GB crni 5G">
                <input type="hidden" name="price" value="9900">
                <button class="button-24" type="submit" id="odaberi1">Dodaj u košaricu <i class="fas fa-arrow-right"></i></button>
            </form>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <br><br><br>
        <h1>&nbsp;&nbsp;&nbsp;O uređaju</h1>
        <br>
        <div class="row row-cols-auto bg-dark">
            <div class="col">
                <br>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/motoe201.png"
                    width="500px" height="350px" class="rounded">
            </div>
            <div class="col">
                <br>
                <br>
                <br>
                <br>
                <br>
                <h2 style="color: white;"><b>Sve što ti treba!</b></h2>
                <p style="color: white;">Upoznaj moto E20. 6,5” Max Vision HD+ zaslon oživljava tvoj omiljeni sadržaj. <br>Lako obavljaj više zadataka i prelazi između aplikacija uz osmojezgreni procesor.</p>
            </div>
        </div>
        <div class="row row-cols-auto bg-dark">
            <div class="col">
                <br>
                <br>
                <br>
                &nbsp;&nbsp;&nbsp; <h2 style="color: white; margin-left: 40px;"><b><br><br>Zaslon koji ostavlja bez daha
                </b></h2>
                <p style="color: white; margin-left: 40px;">Uroni u svoj omiljeni sadržaj i video razgovore na velikom 6,5” Max Vision HD+ zaslonu koji lako stane u jednu ruku.
                </p>
            </div>
            <div class="col"><br><br>
                &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; <img src="../images/motoe202.png" width="600px" height="400px"
                    class="rounded">
            </div>
        </div>
        <div class="row row-cols-auto bg-dark">
            <br><br><br>
            <div class="col"><br><br>
                <br>
                <img src="../images/motoe203.png" width="600px" height="350px" class="rounded" style="margin-left: 30px;">
            </div>
            <div class="col">
                <br>
                <br>
                <br>
                <br>
                <br>
                <h2 style="color: white;"><b>Fokus određuje stvarnost</b></h2>
                <p style="color: white;">Reci zbogom mutnim fotografijama s brzom glavnom kamerom od 13 MP. <br>Snimi portrete profesionalnog izgleda pomoću kamere dubine 2 MP.
                </p>
            </div>
        </div>
        <div class="row row-cols-auto bg-dark">
            <div class="col">
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <h2 style="color: white; margin-left: 40px;"><b>Zaboravi na čekanje</b></h2>
                <p style="color: white; margin-left: 40px;">Brzina bez koje nećeš moć, osmojezgreni procesor omogućavati nevjerojatnu brzinu, <br>a fotografije i videozapisi su oštriji i životopisniji
                </p>
            </div>
            <div class="col"><br><br><br>
                &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; <img src="../images/motoe204.png" width="550px" height="360px"
                    class="rounded"><br><br>
            </div>
        </div>
    </div>
</div>
    <br><br>

    <div class="container">
        <div class="col-lg-12">
            <h1>Tehničke specifikacije</h1>
            <table class="table table-light table-hover">
                <tbody>
                    <tr>
                        <td>Podržane mreže</td>
                        <td>GSM 850, LTE 1800, LTE 2600, 900, 1800, 1900, UMTS 850, UMTS 900, UMTS 1900, UMTS 2100, LTE
                            800 MHz</td>
                    </tr>
                    <tr>
                        <td>Prijenos podataka</td>
                        <td>GPRS, HSDPA 42.2, EDGE, LTE Cat20 2000 Mbps DL, 5G, UMTS, HSUPA, HSUPA 5.76
                        </td>
                    </tr>
                    <tr>
                        <td>Povezivost</td>
                        <td>Bluetooth, NFC, Sinkronizacija s racunalom, USB kabel, WLAN, GPS</td>
                    </tr>
                    <tr>
                        <td>Poruke</td>
                        <td>SMS, MMS, E-mail klijent</td>
                    </tr>
                    <tr>
                        <td>Zaslon</td>
                        <td>7.6-inčni QXGA+ Dynamic AMOLED</td>
                    </tr>
                    <tr>
                        <td>Kamera</td>
                        <td>Trostruka glavna (12 Mpx + 12 Mpx + 12 Mpx) + Selfie (10 Mpx)</td>
                    </tr>
                    <tr>
                        <td>Memorija telefona</td>
                        <td>Radna memorija 12GB, korisnička i sistemska memorija 256GB</td>
                    </tr>
                    <tr>
                        <td>Procesor</td>
                        <td>Qualcomm SM8350 Snapdragon 888 5G (5 nm)</td>
                    </tr>
                    <tr>
                        <td>Dimenzije uređaja</td>
                        <td>Preklopljen: 67.1 x 158.2 x 16mm, Rasklopljen: 128.1 x 158.2 x 6.4 mm</td>
                    </tr>
                    <tr>
                        <td>Težina</td>
                        <td>271 g</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <br>

    <a class="back-to-top"><i class="fas fa-arrow-up"></i></a>

    <!--https://w3schoolweb.com/bootsrap5-footer-with-social-media-icons/-->
    <section class="footer">
        <div class="container">
            <div class="footer__content">
                <div class="footer__heading">
                    <h2>MobilMania</h2>
                </div>
                <p class="mb-0">Copyright &copy; 2021 mato3089@gmail.com</p>

                <ul class="social__media">
                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></li>
                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                </ul>
            </div>
        </div>
    </section>

</body>

</html>