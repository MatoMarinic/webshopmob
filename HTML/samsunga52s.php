<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MobilMania WebShop</title>

    <!--BOOTSTRAP-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"> </script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!---->
    <script src="https://kit.fontawesome.com/4a15fc06a1.js" crossorigin="anonymous"></script>

    <link href="../style/css.css" rel="stylesheet">

    <script src="../js/javascript.js"></script>
</head>

<body>

<?php include('../view/navbar.php'); ?>
    <br>
    <br><br><br>


    <div class="container">
        <div class="row d-flex justify-content-center">
          <div class="col-lg-4 mb-4">
            <div class="card h-100">
              <h4 class="card-header">
            Samsung</h4>
            <p>&nbsp;<b> Samsung Galaxy A52s 5G bijeli</b></p>
          <div class="card-body"> 
            <img src="../images/galaxya52s.jpg" width="350px" height="370px">
               
            <p class="card-text" id="mob1"><br>
           &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;<b>2.490 kn</b><br></p>
           <hr>
           <p class="card-text" id="mob1">&nbsp; &nbsp;&nbsp;<b>Super cijena!</b> </p>
          </div>
        </div>
      </div>
      <div class="col-lg-4 justify-content-lg-center dugme" >
      <form action="../controllers/CartController.php" method="post">
                <input type="hidden" name="name" value="Samsung Galaxy A52s 5G bijeli">
                <input type="hidden" name="price" value="2490">
                <button class="button-24" type="submit" id="odaberi1">Dodaj u košaricu <i class="fas fa-arrow-right"></i></button>
        </form>
      </div>
    </div>
</div>

<br>
<div class="container">
    <br><br><br>
    <h1>O uređaju</h1>
    <br>
    <div class="row bg-dark">
        <div class="col-lg-6"><br>
            <img src="../images/galaxya52skamera.png" width="450px" height="300px" class="rounded">
        </div>
        <div class="col-lg-6"><br>
            <h2 style="color: white;"><b>Novi standard zadivljujućeg dizajna</b></h2>
            <p style="color: white;">Sustav kamera dobiva najveću nadogradnju ikada. iPhone 13 Pro i iPhone 13 Pro Max koriste superinteligentni softver za nove tehnike fotografiranja i snimanja filmova. Po prvi puta Appel je dodao makro snimanje na svim stražnjim kamerama, te također podržava noćni mod (Night Mode). Kamere imaju i 77mm telefoto s 3x optičkim povećanje, te naravno ima glavnu i širokokutnu kameru. Učini svoje fotografije prosefionalnima i iskaži svoju kreativnu stranu!</p>
        </div>
    </div>
    <br><br><br>
    <div class="row">
        <div class="col-lg-6">
          <br><br><br>
            <h2><b>Sjajna kamera, uvijek oštra i mirna</b></h2>
            <p>Kamera telefona Galaxy A52s 5G s višestrukim objektivima tvoje fotografije podiže na sljedeću razinu. Snimaj fotografije izvanredno visoke rezolucije uz 64-megapikselnu glavnu kameru s optičkom stabilizacijom slike (OIS) za oštre i jasne fotografije snimljene u bilo koje doba dana. Proširi kut promatranja Ultraširokokutnom kamerom. Prilagodi fokus uz kameru za percepciju dubine polja ili se približi detaljima zahvaljujući makrokameri.</p>
        </div>
        <div class="col-lg-6">
            <img src="../images/galaxya52skameraa.png" width="650px" height="400px" class="rounded">
        </div>
    </div>
    <br><br>
    <div class="row bg-dark">
      <div class="col-lg-6">  
          <img src="../images/galaxya52szaslon.png" width="600px" height="400px" class="rounded">
      </div>
      <div class="col-lg-6">
        <br><br><br>
          <h2 style="color: white;"><b>Sjajan zaslon, iznimno uglađeno kretanje po zaslonu</b></h2>
          <p style="color: white;">Odmori oči na živopisnim detaljima uz Super AMOLED zaslon FHD+ rezolucije, čija svjetlina doseže 800 nita1 zahvaljujući kojima ćeš sadržaj vidjeti jasno, čak i pri jarkom danjem svjetlu. Eye Comfort Shield2 tehnologija smanjuje razinu plave svjetlosti, a opcija Osobito uglađeno kretanje po zaslonu osigurava ti ugodan pogled bilo da igraš videoigru ili se krećeš po zaslonu. Sve to na širokom zaslonu dijagonale 6,5 inča.</p>
      </div>
  </div>
  <br>
  <div class="row">
    <div class="col-lg-6"><br>
      <br><br><br>
        <h2><b>Snimite svaki detalj, čak i u uvjetima slabog osvjetljenja</b></h2>
        <p>Zaboravi na mutne fotografije i videozapise. OIS (optička stabilizacija slike) pomaže stabilizirati tvoje snimke kako bi pokreti ostali uglađeni, a fotografije jasne — čak i pri slabom osvjetljenju. Kamera uvlači više svjetlosti kako bi dodatno osvijetlila tvoje snimke snimljene u uvjetima slabog osvjetljenja.</p>
    </div>
    <div class="col-lg-6">
        <img src="../images/galaxya52sdetalj.png" width="650px" height="400px" class="rounded">
    </div>
</div>
</div>
<br><br><br>  
<div class="container">
    <div class="col-lg-12">
    <h1>Tehničke specifikacije</h1>        
    <table class="table table-light table-hover">
      <tbody>
        <tr>
          <td>Podržane mreže</td>
          <td>GSM 850, LTE 1800, LTE 2600, 900, 1800, 1900, UMTS 850, UMTS 900, UMTS 1900, UMTS 2100, LTE 800 MHz</td>
        </tr>
        <tr>
          <td>Prijenos podataka</td>
          <td>GPRS, HSDPA 42.2, EDGE, LTE Cat18 1200 Mbps DL, 5G, HSUPA, HSUPA 5.76</td>
        </tr>
        <tr>
            <td>Povezivost</td>
            <td>Bluetooth, NFC, USB kabel, WLAN, GPS</td>
        </tr>
        <tr>
          <td>Poruke</td>
          <td>SMS, MMS, E-mail klijent</td>
        </tr>
        <tr>
          <td>Zaslon</td>
          <td>Super AMOLED 6.5" (glavni zaslon) rezolucije 1080 x 2400</td>
        </tr>
        <tr>
          <td>Kamera</td>
          <td>Četverostruka glavna (64 Mpx + 12 Mpx + 5 Mpx + 5 Mpx) + Selfie (32 Mpx)</td>
        </tr>
        <tr>
          <td>Memorija telefona</td>
          <td>Radna memorija 6GB, korisnička i sistemska memorija 128GB</td>
        </tr>
        <tr>
          <td>Dimenzije uređaja</td>
          <td>159.9 x 75.1 x 8.4 mm</td>
        </tr>
        <tr>
          <td>Procesor</td>
          <td>Qualcomm SM7325 Snapdragon 778G 5G (6 nm)</td>
        </tr> 
        <tr>
          <td>Težina</td>
          <td>189 g</td>
        </tr> 
      </tbody>
    </table>
    </div>
</div>
  
<a class="back-to-top"><i class="fas fa-arrow-up"></i></a>

      <!--https://w3schoolweb.com/bootsrap5-footer-with-social-media-icons/-->
      <section class="footer">
        <div class="container">
          <div class="footer__content">
            <div class="footer__heading">
              <h2>MobilMania</h2>
            </div>
            <p class="mb-0">Copyright &copy; 2021 mato3089@gmail.com</p>
       
            <ul class="social__media">
              <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></li>
              <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
            </ul>
          </div>
        </div>
      </section>
      
</body>
</html>