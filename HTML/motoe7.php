<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MobilMania WebShop</title>

    <!--BOOTSTRAP-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"> </script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!---->
    <script src="https://kit.fontawesome.com/4a15fc06a1.js" crossorigin="anonymous"></script>

    <link href="../style/css.css" rel="stylesheet">

    <script src="../js/javascript.js"></script>
</head>

<body>

<?php include('../view/navbar.php'); ?>
    <br>
    <br><br><br>


    <div class="container">
        <div class="row d-flex justify-content-center">
          <div class="col-lg-4 mb-4">
            <div class="card h-100">
              <h4 class="card-header">
            Motorola</h4>
            <p>&nbsp;<b> Moto E7 dual sim siva</b></p>
          <div class="card-body"> 
            <img src="../images/motoe7.jpg" width="350px" height="350px">
               
            <p class="card-text" id="mob1"><br>
           &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;<b>1.299 kn</b><br></p>
           <hr>
           <p class="card-text" id="mob1">&nbsp; &nbsp;&nbsp;<b>Super cijena!</b> </p>
          </div>
        </div>
      </div>
      <div class="col-lg-4 justify-content-lg-center dugme">
      <form action="../controllers/CartController.php" method="post">
                <input type="hidden" name="name" value="Moto E7 dual sim siva">
                <input type="hidden" name="price" value="1299">
                <button class="button-24" type="submit" id="odaberi1">Dodaj u košaricu <i class="fas fa-arrow-right"></i></button>
        </form>
      </div>
    </div>
</div>

<div class="container-fluid">
    <br><br><br>
    <h1>&nbsp;&nbsp;&nbsp;O uređaju</h1>
    <br>
    <div class="row bg-dark">
        <br><br><br>
        <div class="col-lg-6">
            <br>
            &nbsp;&nbsp;&nbsp;<img src="../images/motoe7mob.jpg" width="500px" height="350px" class="rounded"> 
            <br>
        </div>
        <div class="col-lg-6">
            <br>
            <br>
            <h2 style="color: white;">Oživi noć uz Moto E7</h2>
            <p style="color: white;">Snimaj oštrije, svjetlije fotografije i videozapise danju ili noću s 48 MP sustavom dvostrukih kamera. Omogući svojim omiljenim igrama i filmovima široki zaslon kakav zaslužuju na 6,5 ​​"Max Vision HD + zaslonu. Uz to, zabavi se cijeli dan i noć s 36-satnom baterijom i osmojezgrenim procesorom koji te neće usporiti.</p> 
        </div>        
    </div>
    <div class="row bg-dark">
        <div class="col-lg-6">
            <br>
            <br>
            &nbsp;&nbsp;&nbsp; <h2 style="color: white;">Neka svako okidanje bude savršeno</h2>
            <p style="color: white;">Senzor od 48 MP znači da tvoje fotografije postaju oštre i sjajne u bilo kojem svjetlu, a Quad Pixel tehnologija pruža 4x bolju osjetljivost pri slabom osvjetljenju. Približi se 2,5 puta bliže subjektima i istraži najfinije detalje pomoću kamere Macro Vision.</p>
        </div>
        <div class="col-lg-6">
            <img src="../images/motoe7mobb.jpg" width="650px" height="350px" class="rounded">
        </div>
    </div>
    <div class="row bg-dark">
        <br><br><br>
        <div class="col-lg-6">
            
            <img src="../images/motoe7mobbb.jpg" width="500px" height="350px" class="rounded"> 
        </div>
        <div class="col-lg-6">
            <br>
            <br>
            <h2 style="color: white;">Danju. Noću. Bilo kada.</h2>
            <p style="color: white;">Zabrinut si jer rasvjeta nije dobra za sjajnu fotografiju? Ne trebaš biti. Svaki put ćeš postići zapanjujuće rezultate sa senzorom od 48 MP. Osim toga, prebaci se na način noćnog vida kako bi noću uživao u više detalja.</p> 
        </div>
    </div>
    <div class="row bg-dark">
        <div class="col-lg-6">
            <br>
            <br>
            <h2 style="color: white;">Široki zaslon donosi pobjedu</h2>
            <p style="color: white;">Igre, filmovi i video razgovori puno su zabavniji na ultraširokom ekranu. 6.5 "Max Vision HD + zaslon ima omjer slike 20: 9 s velikim udjelom zaslona i tijela. Pogledaj više. Igraj više. Uživaj više.</p>
        </div>
        <div class="col-lg-6">
            <img src="../images/motoe7zaslon.jpg" width="650px" height="360px" class="rounded"><br>
        </div>
    </div>

</div>
<br><br>

<div class="container">
    <div class="col-lg-12">
    <h1>Tehničke specifikacije</h1>        
    <table class="table table-light table-hover">
      <tbody>
        <tr>
          <td>Podržane mreže</td>
          <td>GSM 850, LTE 1800, LTE 2600, 900, 1800, 1900, UMTS 850, UMTS 900, UMTS 1900, UMTS 2100, LTE 800 MHz</td>
        </tr>
        <tr>
          <td>Prijenos podataka	</td>
          <td>GPRS, HSDPA 42.2, EDGE, LTE Cat4 150 Mbps DL, UMTS, HSUPA 5.76</td>
        </tr>
        <tr>
            <td>Povezivost</td>
            <td>Bluetooth, NFC, USB kabel, WLAN, GPS</td>
        </tr>
        <tr>
            <td>Zaslon</td>
            <td>720 x 1600 piksela HD+ IPS LCD 6.5 inča</td>
        </tr>
        <tr>
            <td>Kamera</td>
            <td>1Mpx</td>
        </tr>
        <tr>
            <td>Memorija telefona</td>
            <td>Radna memorija 2GB, korisnička i sistemska memorija 32GB</td>
        </tr>
        <tr>
            <td>Dimenzije telefona</td>
            <td>164.9 x 75.7 x 8.9 mm</td>
        </tr>
        <tr>
            <td>Procesor</td>
            <td>MediaTek Helio G25 (12 nm)</td>
        </tr>
        <tr>
            <td>Pakiranje</td>
            <td>USB kabel</td>
        </tr>
        <tr>
            <td>Težina</td>
            <td>180 g</td>
        </tr>
      </tbody>
    </table>
    </div>
</div>
<br>
  
<a class="back-to-top"><i class="fas fa-arrow-up"></i></a>

      <!--https://w3schoolweb.com/bootsrap5-footer-with-social-media-icons/-->
      <section class="footer">
        <div class="container">
          <div class="footer__content">
            <div class="footer__heading">
              <h2>MobilMania</h2>
            </div>
            <p class="mb-0">Copyright &copy; 2021 mato3089@gmail.com</p>
       
            <ul class="social__media">
              <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></li>
              <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
            </ul>
          </div>
        </div>
      </section>
      
</body>
</html>