<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MobilMania WebShop</title>

    <!--BOOTSTRAP-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"> </script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!---->
    <script src="https://kit.fontawesome.com/4a15fc06a1.js" crossorigin="anonymous"></script>

    <link href="../style/css.css" rel="stylesheet">

    <script src="../js/javascript.js"></script>
</head>

<body>

  <?php include('../view/navbar.php'); ?>
    <br>
    <br><br><br>


    <div class="container">
        <div class="row d-flex justify-content-center">
          <div class="col-lg-4 mb-4">
            <div class="card h-100">
              <h4 class="card-header">
            Samsung</h4>
            <p>&nbsp;<b>Samsung Galaxy A03s crni</b></p>
          <div class="card-body"> 
            <img src="../imagesa03s.jpg" width="350px" height="350px">
               
            <p class="card-text" id="mob1"><br>
           &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;<b>1.898,18 kn</b><br></p>
           <hr>
           <p class="card-text" id="mob1">&nbsp; &nbsp;&nbsp;<b>Super cijena!</b> </p>
          </div>
        </div>
      </div>
      <div class="col-lg-4 justify-content-lg-center dugme">
        <form action="../controllers/CartController.php" method="post">
            <input type="hidden" name="name" value="Samsung Galaxy A03s crni">
            <input type="hidden" name="price" value="1898.18">
            <button class="button-24" type="submit" id="odaberi1">Dodaj u košaricu <i class="fas fa-arrow-right"></i></button>
          </form>
        
      </div>
    </div>
</div>

<div class="container-fluid">
    <br><br><br>
    <h1>&nbsp;&nbsp;&nbsp;O uređaju</h1>
    <br>
    <div class="row bg-dark">
        <br><br><br>
        <div class="col-lg-6">
            <br>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="../imagesa03s1.png" width="500px" height="350px" class="rounded"> 
            <br>
        </div>
        <div class="col-lg-6">
            <br>
            <br>
            <br>
            <br>
            <h2 style="color: white;">Blag prema tvojim očima i rukama</h2>
            <p style="color: white;">Galaxy A03s kombinira klasične boje s izgledom i doživljajem korištenja nježnim na dodir. <br>Profinjene zaobljene linije čine ga ugodnim za držanje u ruci i omogućuju jednostavno kretanje zaslonom.</p> 
        </div>        
    </div>
    <div class="row bg-dark">
        <div class="col-lg-6">
            <br>
            <br>
            &nbsp;&nbsp;&nbsp; <h2 style="color: white; margin-left: 20px;">Zabilježi svoj svijet na razne načine uz pomoć trostruke kamere</h2>
            <p style="color: white; margin-left: 20px;">Detaljno zabilježi trenutke za pamćenje uz pomoć glavne kamere od 13 MP. Prilagodi fokus uz kameru za percepciju dubine polja ili se približi detaljima uz makrokameru.</p>
        </div>
        <div class="col-lg-6">
            <img src="../imagesa03s2.png" width="650px" height="400px" class="rounded">
        </div>
    </div>
    <div class="row bg-dark">
        <br><br><br>
        <div class="col-lg-6"><br><br>
            
            <img src="../imagesa03s3.png" width="500px" height="350px" class="rounded" style="margin-left: 30px;"> 
        </div>
        <div class="col-lg-6">
            <br>
            <br>
            <br>
            <h2 style="color: white;">Snaga uz koju nastavljaš dalje</h2>
            <p style="color: white;">Zadrži prednost u danu uz bateriju koja Vte neće sputavati. Baterija kapaciteta 5000 mAh (uobičajeno) <br>omogućuje ti da nastaviš s radom satima.</p> 
        </div>
    </div>
    <div class="row bg-dark">
        <div class="col-lg-6">
            <br>
            <br>
            <br>
            <h2 style="color: white; margin-left: 20px;">Omogući si više prostora za igru</h2>
            <p style="color: white; margin-left: 20px;">Galaxy A03s kombinira snagu osmojezgrenog Octa-core procesora s do 3 GB/4 GB RAM-a za brzo i djelotvorno obavljanje zadataka. Uživaj u 32 GB/64 GB unutarnje memorije ili dodaj još više prostora uz MicroSD karticu kapaciteta do 1 TB.</p>
        </div>
        <div class="col-lg-6"><br><br>
            <img src="../imagesa03s4.png" width="600px" height="360px" class="rounded"><br><br>
        </div>
    </div>
</div>
<br><br>

<div class="container">
    <div class="col-lg-12">
    <h1>Tehničke specifikacije</h1>        
    <table class="table table-light table-hover">
      <tbody>
        <tr>
            <td>Podržane mreže</td>
            <td>GSM 850, LTE 1800, LTE 2600, 900, 1800, 1900, UMTS 850, UMTS 900, UMTS 1900, UMTS 2100, LTE 800 MHz</td>
          </tr>
        <tr>
          <td>Prijenos podataka</td>
          <td>GPRS, HSDPA 42.2, EDGE, LTE Cat4 150 Mbps DL, HSUPA, HSUPA 5.76</td>
        </tr>
        <tr>
          <td>Povezivost</td>
          <td>Bluetooth, USB kabel, WLAN, GPS</td>
        </tr>
        <tr>
            <td>Poruke</td>
            <td>SMS, MMS, E-mail klijent</td>
        </tr>
        <tr>
            <td>Kamera</td>
            <td>Trostruka glavna (13 Mpx + 2 Mpx + 2 Mpx) + Selfie (5 Mpx)</td>
        </tr>
        <tr>
            <td>Memorija telefona</td>
            <td>Radna memorija 3GB, korisnička i sistemska memorija 32GB</td>
        </tr>
        <tr>
            <td>Procesor</td>
            <td>MediaTek MT6765 Helio P35 (12nm)</td>
        </tr>
        <tr>
            <td>Dimenzije uređaja</td>
            <td>164.2 x 75.9 x 9.1 mm</td>
        </tr>
        <tr>
            <td>Težina</td>
            <td>196 g</td>
        </tr>
      </tbody>
    </table>
    </div>
</div>
<br>
  
<a class="back-to-top"><i class="fas fa-arrow-up"></i></a>

      <!--https://w3schoolweb.com/bootsrap5-footer-with-social-media-icons/-->
      <section class="footer">
        <div class="container">
          <div class="footer__content">
            <div class="footer__heading">
              <h2>MobilMania</h2>
            </div>
            <p class="mb-0">Copyright &copy; 2021 mato3089@gmail.com</p>
       
            <ul class="social__media">
              <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></li>
              <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
            </ul>
          </div>
        </div>
      </section>
      
</body>
</html>