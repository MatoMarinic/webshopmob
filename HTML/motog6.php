<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MobilMania WebShop</title>

    <!--BOOTSTRAP-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"> </script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!---->
    <script src="https://kit.fontawesome.com/4a15fc06a1.js" crossorigin="anonymous"></script>

    <link href="../style/css.css" rel="stylesheet">

    <script src="../js/javascript.js"></script>
</head>

<body>

<?php include('../view/navbar.php'); ?>
    <br>
    <br><br><br>


    <div class="container">
        <div class="row d-flex justify-content-center">
          <div class="col-lg-4 mb-4">
            <div class="card h-100">
              <h4 class="card-header">
            Motorola</h4>
            <p>&nbsp;<b> Moto G60 siva</b></p>
          <div class="card-body"> 
            <img src="../images/moto60.jpg" width="350px" height="350px">
               
            <p class="card-text" id="mob1"><br>
           &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;<b>2.099 kn</b><br></p>
           <hr>
           <p class="card-text" id="mob1">&nbsp; &nbsp;&nbsp;<b>Super cijena!</b> </p>
          </div>
        </div>
      </div>
      <div class="col-lg-4 justify-content-lg-center dugme" >
      <form action="../controllers/CartController.php" method="post">
                <input type="hidden" name="name" value="Moto G60 siva">
                <input type="hidden" name="price" value="2099">
                <button class="button-24" type="submit" id="odaberi1">Dodaj u košaricu <i class="fas fa-arrow-right"></i></button>
        </form>
      </div>
    </div>
</div>

<br>
<div class="container">
    <br><br><br>
    <h1>O uređaju</h1>
    <br>
    <div class="row">
        <br><br><br>
        <div class="col-lg-6">
            
            <img src="../images/motorolag60.png" width="500px" height="350px" class="rounded"> 
        </div>
        <div class="col-lg-6">
            <br>
            <br>
            <h2>Pametan i moderan dizajn</h2>
            <p>Detalji poput zatamnjenog kućišta na stražnjim fotoaparatima i iznimno tanki prsteni mobitelu moto g60 daju izgled i osjet sofisticiranosti i promišljenog dizajna. Nadalje, dizajn koji odbija vodu i štiti tvoj telefon od prolijevanja i prskanja tekućine.</p> 
        </div>

        
    </div>
    <div class="row">
        <div class="col-lg-6">
            <br>
            <br>
            <h2>Za savšeni trenutak, savršena slika</h2>
            <p>Zahvaljujući mobitelu moto G60, vodeći sustav fotoaparata u klasi nalazi se na dohvat ruke. Senzor od 108 MP koristi 9 x veće Ultra piksele kako bi pružio 9 x bolju osjetljivost na svjetlo i fotografije visoke rezolucije bez premca.</p>
        </div>
        <div class="col-lg-6">
            <img src="../images/motorolag60kameara.jpg" width="650px" height="350px" class="rounded">
        </div>
    </div>
    <div class="row">
        <br><br><br>
        <div class="col-lg-6">
            
            <img src="../images/motorolamemorija.jpg" width="500px" height="350px" class="rounded"> 
        </div>
        <div class="col-lg-6">
            <br>
            <br>
            <h2>Memorija sa kojom nema brisanja</h2>
            <p>Uz 128 GB interne memorije4, imaš hrpu prostora za fotografije, filmove, pjesme, aplikacije i igre. Uvijek možeš dodati do 1 TB dodatnog prostora pomoću microSD kartice5.</p> 
        </div>
</div>

</div>
<br><br>

<div class="container">
    <div class="col-lg-12">
    <h1>Tehničke specifikacije</h1>        
    <table class="table table-light table-hover">
      <tbody>
        <tr>
          <td>Podržane mreže</td>
          <td>GSM 850, LTE 1800, LTE 2600, 900, 1800, 1900, UMTS 850, UMTS 900, UMTS 1900, UMTS 2100, LTE 800 MHz</td>
        </tr>
        <tr>
          <td>Prijenos podataka	</td>
          <td>GPRS, HSDPA 42.2, EDGE, UMTS, HSUPA, HSUPA 5.76</td>
        </tr>
        <tr>
            <td>Povezivost</td>
            <td>Bluetooth, NFC, USB kabel, WLAN, GPS</td>
        </tr>
        <tr>
            <td>Zaslon</td>
            <td>6.8" Max Vision zaslon</td>
        </tr>
        <tr>
            <td>Kamera</td>
            <td>Trostruka glavna (108 Mpx + 8 Mpx + 2 Mpx) + Selfie (32 Mpx)</td>
        </tr>
        <tr>
            <td>Memorija telefona</td>
            <td>adna memorija 6GB, korisnička i sistemska memorija 128GB</td>
        </tr>
        <tr>
            <td>Dimenzije telefona</td>
            <td>169.6 x 75.9 x 9.8 mm</td>
        </tr>
        <tr>
            <td>Procesor</td>
            <td>Qualcomm SM7150 Snapdragon 732G (8 nm)</td>
        </tr>
        <tr>
            <td>Pakiranje</td>
            <td>vodič za brzi početak, Punjač, Zaštitni preklop, USB kabel</td>
        </tr>
      </tbody>
    </table>
    </div>
</div>
<br>
  
<a class="back-to-top"><i class="fas fa-arrow-up"></i></a>

      <!--https://w3schoolweb.com/bootsrap5-footer-with-social-media-icons/-->
      <section class="footer">
        <div class="container">
          <div class="footer__content">
            <div class="footer__heading">
              <h2>MobilMania</h2>
            </div>
            <p class="mb-0">Copyright &copy; 2021 mato3089@gmail.com</p>
       
            <ul class="social__media">
              <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></li>
              <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
            </ul>
          </div>
        </div>
      </section>
      
</body>
</html>