<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MobilMania WebShop</title>

    <!--BOOTSTRAP-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"> </script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!---->
    <script src="https://kit.fontawesome.com/4a15fc06a1.js" crossorigin="anonymous"></script>

    <link href="../style/css.css" rel="stylesheet">

    <script src="../js/javascript.js"></script>
</head>

<body>

<?php include('../view/navbar.php'); ?>
    <br>
    <br><br><br>


    <div class="container">
        <div class="row d-flex justify-content-center">
          <div class="col-lg-4 mb-4">
            <div class="card h-100">
              <h4 class="card-header">
            Huwaei</h4>
            <p>&nbsp;<b>Huawei P40 lite zeleni 128GB</b></p>
          <div class="card-body"> 
            <img src="../images/huaweip40lite.jpg" width="350px" height="350px">
               
            <p class="card-text" id="mob1"><br>
           &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;<b>1.898,18 kn</b><br></p>
           <hr>
           <p class="card-text" id="mob1">&nbsp; &nbsp;&nbsp;<b>Super cijena!</b> </p>
          </div>
        </div>
      </div>
      <div class="col-lg-4 justify-content-lg-center dugme">
      <form action="../controllers/CartController.php" method="post">
                <input type="hidden" name="name" value="Huawei P40 lite zeleni 128GB">
                <input type="hidden" name="price" value="1898.18">
                <button class="button-24" type="submit" id="odaberi1">Dodaj u košaricu <i class="fas fa-arrow-right"></i></button>
        </form>
      </div>
    </div>
</div>

<div class="container-fluid">
    <br><br><br>
    <h1>&nbsp;&nbsp;&nbsp;O uređaju</h1>
    <br>
    <div class="row bg-dark">
        <br><br><br>
        <div class="col-lg-6">
            <br>
            &nbsp;&nbsp;&nbsp;<img src="../images/p40lite.png" width="500px" height="350px" class="rounded"> 
            <br>
        </div>
        <div class="col-lg-6">
            <br>
            <br>
            <h2 style="color: white;">Svjež, racionalan dizajn</h2>
            <p style="color: white;">HUAWEI P40 lite dizajn naglašava bitne stvari, bez nepotrebnih trikova. Kućište je zakrivljeno sa četiri strane, tako da u ruci leži dobro kao što i izgleda dobro. Također, s bočno ugrađenim dizajnom otiska prsta, možeš ga otključati jednim dodirom. Ovdje pogledaj kako instalirati aplikacije na svom novom Huaweiu.</p> 
        </div>        
    </div>
    <div class="row bg-dark">
        <div class="col-lg-6">
            <br>
            <br>
            &nbsp;&nbsp;&nbsp; <h2 style="color: white;">Neka tvoja osobnost zablista</h2>
            <p style="color: white;">U super Night Selfie 2.0 načinu rada , Huawei P40 lite automatski osvjetljava tvoje lice kad snimaš selfie na slabo osvjetljenim mejstima i optimizira osvjetljenje, zasićenost i kontrast pozadine kako bi te istaknuo. Pritom koristi Multi-Frame Fusion tehnologiju za kombiniranje više snimaka.</p>
        </div>
        <div class="col-lg-6">
            <img src="../images/p40lite1.png" width="650px" height="400px" class="rounded">
        </div>
    </div>
    <div class="row bg-dark">
        <br><br><br>
        <div class="col-lg-6">
            
            <img src="../images/p40lite2.png" width="500px" height="350px" class="rounded"> 
        </div>
        <div class="col-lg-6">
            <br>
            <br>
            <h2 style="color: white;">Uvijek spremni na igru.</h2>
            <p style="color: white;">Unutar HUAWEI P40 lite pametnog telefona ima dovoljno snage za pokretanje čak i najzahtjevnijih igara. Nova generacija GPU Turbo nudi HD grafiku i visoku brzinu bez zastakivanja, pružajući glatko iskustvo u svakom trenutku igranja. Također, istovremeno troši iznenađujuće malo energije, tako da možeš duže i bezbrižnije igrati.</p> 
        </div>
    </div>
    <div class="row bg-dark">
        <div class="col-lg-6">
            <br>
            <br>
            <h2 style="color: white;">Dugotrajna baterija s brzim punjenjem</h2>
            <p style="color: white;">S velikim kapacitetom baterije od 4.200 mAh. Prikopčaj mobitel dok piješ jutarnju kavu i bit će napunjen u trenutku, zahvaljujući TÜV Rheinland certificiranoj HUAWEI 40 W SuperCharge tehnologiji, koja u 30 minuta napuni do 70% baterije.</p>
        </div>
        <div class="col-lg-6">
            <img src="../images/p40lite3.png" width="650px" height="360px" class="rounded"><br>
        </div>
    </div>
    <div class="row bg-dark">
        <br><br><br><br>
        <div class="col-lg-6">
            <br>
            <img src="../images/p40lite4.png" width="500px" height="400px" class="rounded"> 
        </div>
        <div class="col-lg-6">
            <br>
            <br>
            <br>
            <h2 style="color: white;">Sučelje koje ćeš rado koristiti</h2>
            <p style="color: white;">HUAWEI P40 lite dolazi s našim najboljim sučeljem do sada, EMUI10. Dizajn smo unaprijedili i učinili čisćim i intuitivnijim s elegantnim gradijentom boja. Sve se nalazi tamo gdje bi trebalo biti, a s prirodnim prijelazima između izbornika odmah možeš doći tamo gdje želiš biti. Upoznaj pravi smisao multitaskinga!</p> 
        </div>
    </div>

</div>
<br><br>

<div class="container">
    <div class="col-lg-12">
    <h1>Tehničke specifikacije</h1>        
    <table class="table table-light table-hover">
      <tbody>
        <tr>
          <td>Prijenos podataka</td>
          <td>GPRS, HSDPA 42.2, EDGE, LTE Cat9 450 Mbps DL, UMTS, HSUPA 5.76</td>
        </tr>
        <tr>
          <td>Povezivost</td>
          <td>Bluetooth, NFC, Sinkronizacija s racunalom, USB kabel, WLAN</td>
        </tr>
        <tr>
            <td>Poruke</td>
            <td>SMS, MMS, E-mail klijent</td>
        </tr>
        <tr>
            <td>Kamera</td>
            <td>Četverostruka glavna (48 Mpx + 8 Mpx + 2 Mpx + 2 Mpx) + Selfie (20 Mpx)</td>
        </tr>
        <tr>
            <td>Memorija telefona</td>
            <td>Radna memorija 6GB, korisnička i sistemska memorija 128GB</td>
        </tr>
        <tr>
            <td>Procesor</td>
            <td>HiSilicon Kirin 810 (7 nm)</td>
        </tr>
        <tr>
            <td>Težina</td>
            <td>183 g</td>
        </tr>
      </tbody>
    </table>
    </div>
</div>
<br>
  
<a class="back-to-top"><i class="fas fa-arrow-up"></i></a>

      <!--https://w3schoolweb.com/bootsrap5-footer-with-social-media-icons/-->
      <section class="footer">
        <div class="container">
          <div class="footer__content">
            <div class="footer__heading">
              <h2>MobilMania</h2>
            </div>
            <p class="mb-0">Copyright &copy; 2021 mato3089@gmail.com</p>
       
            <ul class="social__media">
              <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></li>
              <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
            </ul>
          </div>
        </div>
      </section>
      
</body>
</html>