<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
    
        <title>MobilMania WebShop</title>
    
        <!--BOOTSTRAP-->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">
    
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"> </script>
    
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    
        <!---->
        <script src="https://kit.fontawesome.com/4a15fc06a1.js" crossorigin="anonymous"></script>
    
        <link href="style/onama.css" rel="stylesheet">
        <script src="js/javascript.js"></script>
</head>
<body> 
<?php include('navbar.php'); ?>
<br><br><br><br>

<div class="container">
    <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
        <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">O nama</li>
        </ol>
    </nav>
</div>

<div class="aboutus-section">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="aboutus">
                    <h2 class="aboutus-title">O nama</h2>
                    <p class="aboutus-text">Tvrtka MobilMania osnovana je 2021.godine s ciljem omogućavanja prodaje mobitela i ostalih usluga. Tvrtka se nalazi u Osijeku te imamo oko 20 zaposlenih ljudi.</p>
                    <p class="aboutus-text">Svakim danom tvrtka se razvija i nastojimo zaposliti što više ljudi i omogućiti komunikaciju s najboljim modelima mobitela.</p>
                    <p class="aboutus-text">U našoj web trgovini pronaći će te puno modela mobitela od kojih su međuostalom Samsung, iPhone, Xiaomi koji su trend u današnje vrijeme. </p>
                    <p class="aboutus-text">Komunikacija je ne izbježan korak u stvaranju nečega novog pa zato smo mi tu da vam je i omogučimo!</p>                
                </div>
            </div>
            <div class="col-md-3 col-sm-6 ">
                <div class="aboutus-banner">
                    <img src="http://themeinnovation.com/demo2/html/build-up/img/home1/about1.jpg" alt="">
                </div>
            </div>
            <div class="col-md-5 col-sm-6 ">
                <div class="feature">
                    <div class="feature-box">
                        <div class="clearfix">
                            <div class="iconset">
                                <span class="glyphicon glyphicon-cog icon"></span>
                            </div>
                            <div class="feature-content">
                                <h4>Radimo s ljubavlju</h4>
                                <p>Omogućavamo komunikaciju na što boljoj razini te tako povezujemo ljude.</p>
                            </div>
                        </div>
                    </div>
                    <div class="feature-box">
                        <div class="clearfix">
                            <div class="iconset">
                                <span class="glyphicon glyphicon-cog icon"></span>
                            </div>
                            <div class="feature-content">
                                <h4>Pouzdani servis</h4>
                                <p>Naši tehničari su majstori u svom poslu tako da vam mobiel popravimo u roku od 24 sata</p>
                            </div>
                        </div>
                    </div>
                    <div class="feature-box">
                        <div class="clearfix">
                            <div class="iconset">
                                <span class="glyphicon glyphicon-cog icon"></span>
                            </div>
                            <div class="feature-content">
                                <h4>Odlična podrška</h4>
                                <p>Ako imate bilo kakvih pitanja u vezi mobitela ili vam nešto nevalja, slobodno nas nazovite. Tu smo za vas!</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<br><br><br><br><br>
<!--https://w3schoolweb.com/bootsrap5-footer-with-social-media-icons/-->
<section class="footer">
  <div class="container">
    <div class="footer__content">
      <div class="footer__heading">
        <h2>MobilMania</h2>
      </div>
      <p class="mb-0">Copyright &copy; 2021 mato3089@gmail.com</p>
 
      <ul class="social__media">
        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
        <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></li>
        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
        <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
      </ul>
    </div>
  </div>
</section>
</body>
</html>