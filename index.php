<?php
include_once 'connection.php';
session_start();
?>

<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MobilMania WebShop</title>

    <!--BOOTSTRAP-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"> </script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!---->
    <script src="https://kit.fontawesome.com/4a15fc06a1.js" crossorigin="anonymous"></script>

    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.13.0/themes/base/jquery-ui.css">

    <link href="style/css.css" rel="stylesheet">

    <script src="addToCart.js"></script>

    <script src="filter.js"></script>
    <script src="priceRange.js"></script>

    <script src="js/javascript.js"></script>
</head>

<body>

    <style>
    .img-responsive {
        border: 0;
    }

    .zoom {
        padding: 20px;
        transition: transform .1s;
        margin: 0 auto;
    }

    .zoom:hover {
        -ms-transform: scale(1.5);
        /* IE 9 */
        -webkit-transform: scale(1.5);
        /* Safari 3-8 */
        transform: scale(1.10);
    }

    #filter {
        background-color: #e6e6e6;
        color: black;
    }

    #price_range {
        height: 10px;
    }

    .list-group-item {
        background-color: #e6e6e6;
    }

    marquee {
        padding: 10px;
        background-color: #e6e6e6;
        font-size: 1.2em;
        height: 50px;
    }

    .button2 {
        display: inline-block;
        padding: 0.5em 3em;
        border: 0.16em solid #FFFFFF;
        margin: 0 0.3em 0.3em 0;
        box-sizing: border-box;
        text-decoration: none;
        text-transform: uppercase;
        font-family: 'Roboto', sans-serif;
        font-weight: 400;
        color: #FFFFFF;
        text-align: center;
        transition: all 0.15s;
    }

    .button2:hover {
        color: #DDDDDD;
        border-color: #DDDDDD;
    }

    a.button2:active {
        color: #BBBBBB;
        border-color: #BBBBBB;
    }

    @media all and (max-width:30em) {
        a.button2 {
            display: block;
            margin: 0.4em auto;
        }
    }
    </style>
    <?php include('navbar.php'); ?>
    <div id="demo" class="carousel slide" data-bs-ride="carousel">

        <!-- Indicators/dots -->
        <div class="carousel-indicators">
            <button type="button" data-bs-target="#demo" data-bs-slide-to="0" class="active"></button>
            <button type="button" data-bs-target="#demo" data-bs-slide-to="1"></button>
            <button type="button" data-bs-target="#demo" data-bs-slide-to="2"></button>
        </div>

        <!-- The slideshow/carousel -->
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="images/mob1.jpg" alt="image" class="d-block" style="width:100%">
                <div class="carousel-caption">
                    <h3>Istražite moć 4K kamere</h3>
                    <a href="ponuda.php"><button class="button-24" role="button">Provjeri ponudu</button></a>
                </div>
            </div>
            <div class="carousel-item">
                <img src="images/s20_mobile.jpg" alt="image" class="d-block" style="width:100%">
                <div class="carousel-caption">
                    <h3>Nevjerojatne performanse koje će vas oduševiti</h3>
                    <a href="ponuda.php"><button class="button-24" role="button">Provjeri ponudu</button></a>
                </div>
            </div>
            <div class="carousel-item">
                <img src="images/tkkooo.jpg" alt="image" class="d-block" style="width:100%">
                <div class="carousel-caption">
                    <h3>Prelijepi dizajn ostavit će vas bez daha</h3>
                    <a href="ponuda.php"><button class="button-24" role="button">Provjeri ponudu</button></a>
                </div>
            </div>
        </div>

        <!-- Left and right controls/icons -->
        <button class="carousel-control-prev" type="button" data-bs-target="#demo" data-bs-slide="prev">
            <span class="carousel-control-prev-icon"></span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#demo" data-bs-slide="next">
            <span class="carousel-control-next-icon"></span>
        </button>
    </div>
    <marquee><b>Napomena!</b> Da bi ste mogli kupovati u našem webshopu morate se <a
            href="registracija.php">registrirati</a></marquee>
    <br><br>

    <h1 class="text-center">Dobrodošli u MobilMania Webshop</h1>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-xs-4 zoom">
                <a href="samsung.php"><img src="images/samsunglogo.png" class="img-responsive"></a>
            </div>
            <div class="col-lg-4 col-md-4 col-xs-4 zoom">
                <a href="xiaomi.php"><img src="images/xiaomilogo.png" class="img-responsive"></a>
            </div>
            <div class="col-lg-4 col-md-4 col-xs-4 zoom">
                <a href="iphone.php"><img src="images/iphonelogo.png" class="img-responsive"></a>
            </div>
            <div class="col-lg-4 col-md-4 col-xs-4 zoom">
                <a href="huawei.php"><img src="images/huaweilogo.png" class="img-responsive"></a>
            </div>
            <div class="col-lg-4 col-md-4 col-xs-4 zoom">
                <a href="motorola.php"><img src="images/motorolalogo.png" class="img-responsive"></a>
            </div>
            <div class="col-lg-4 col-md-4 col-xs-4 zoom">
                <a href="oneplus.php"><img src="images/onepluslogo.png" class="img-responsive"></a>
            </div>
        </div>
    </div>

    <br>

    <div class="container-fluid">
        <div class="row">

            <div class="col-md-3">
                <div id="filter" class="p-2 ms-md-4 ms-sm-2 " style="border: 1px solid black; border-radius:10px;">
                    <div class="border-bottom h5 text-uppercase text-center ">Filter</div>
                    <div class="box border-bottom">

                        <div class="box-label text-uppercase d-flex align-items-center">Cijena [kn]</div>
                        <input type="hidden" id="hidden_minimum_price" value="0">
                        <input type="hidden" id="hidden_maximum_price" value="15000">
                        <p id="price_show">0 - 15000
                        </p>
                        <div id="price_range"></div>
                    </div>
                    <br>

                    <div class="box border-bottom ">
                        <div class="box-label text-uppercase d-flex align-items-center">Proizvođač</div>
                        <?php
                        $query = "SELECT DISTINCT(manufacturer) FROM products";
                        $result = $conn->query($query);
                        $result->fetch_all(MYSQLI_ASSOC);

                        foreach ($result as $row) {
                        ?>
                        <div class="list-group-item">
                            <label onselectstart="return false"><input type="checkbox"
                                    class="common_selector proizvodjac" value="<?php echo $row['manufacturer']; ?>">
                                <?php echo $row['manufacturer']; ?></label>

                        </div>
                        <?php
                        }
                        ?>
                    </div>
                </div>

                <div class="col-md-9">
                    <br>
                    <div class="row filter_data">

                    </div>
                </div>
            </div>

            <!--OVDJE DODATI GALERIJU SLIKA I BUTTON-->
            <div class="col-md-9">
                <div class="row">

                    <?php
                    $sql = "SELECT manufacturer, model, zaslon, procesor, ram, baterija, price, tip, id, image FROM products WHERE tip='Novo' OR tip='Popularno'";
                    $result = mysqli_query($conn, $sql);

                    if (mysqli_num_rows($result) > 0) {
                        $index_id = 0;
                        while ($row = mysqli_fetch_assoc($result)) {
                            echo '
                        <div class="col-lg-3 mb-4" id="product_' . $index_id . '">
                        <div class="card h-100">
                        <h4 class="card-header text-center"><span id="product_' . $index_id . '_manufacturer">' . $row['manufacturer'] . '</span></h4>
                        <p class="text-center">' . $row['model'] . '</p> 
                        <div class="card-body">
                        <img src="images/' . $row['image'] . '" class="img-responsive img-thumbnail" alt="image">
                        <p class="card-text" id="mob1"><br>
                        <ul>
                        <li>Zaslon: ' . $row['zaslon'] . '</li>
                        <li>Procesor: ' . $row['procesor'] . '</li>
                        <li>RAM: ' . $row['ram'] . '</li>
                        <li>Baterija: ' . $row['baterija'] . '</li>
                        </ul>   
                        <br>
                        <p class="text-center"><b><span id="product_' . $index_id . '_price">' . $row['price'] . '</span>,00 kn</b><br></p>';
                            if (isset($_SESSION['loggedin']) && $_SESSION['loggedin']) {
                                echo    '<hr>
                        <p class="text-center"><a class="addToCart" href="addToCart.php?id=' . $row['id'] . '" onclick="appendId(' . $row['id'] . ')"><button class="button-24" role="button">Dodaj u košaricu <i class="fas fa-arrow-right"></i></button></a></p>';
                            }
                            echo                            '</div>
                        </div>
                        </div>';
                            $index_id++;
                        }
                    } else {
                        echo "0 results";
                    }
                    mysqli_close($conn); ?>
                </div>
            </div>
        </div>
    </div>
    <br>

    <div class="container-fluid bg-dark text-white">
        <div class="row">
            <div class="col-lg-6">
                <br>
                <br>
                <h1 style="margin-left:15px;">MOBILMANIA BLOG</h1>
                <br>
                <br>
                <br>
                <h2 style="margin-left:15px;">Best buy mjeseca!</h2>
                <p style="margin-left:15px;">
                    Kupi Galaxy Fold, Galaxy Fold2, ili neki mobitel iz Note20 serije ili S20 serije i na poklon
                    dobiješ Galaxy Watch u vrijednosti od 2.000kn!</p>

                <img id="myImg" class="img-fluid rounded" src="images/poklon.jpg" alt="Ponuda mjeseca" style="margin-left:15px;">
               <br>
               <br>
               <br>
                <img src="images/matexs1.jpg" width="600px" height="400px" style="margin-left:15px;">
            
                <p style="margin-left:15px;">NOVOSTI</p>
                <h4 style="margin-left:15px;">Uz kupovinu novog Huawei MateXS Pro na poklon dobijate sat u vrijednosti 2.000kn!</h4>
                <a href="matexs.php" class="button2" style="margin-left:15px;">Pročitaj više</a>
                <p>
            </div>
            <div class="col-lg-6">
                <br>
                <br>
                <img src="images/s21ultra1.jpg" width="400px" height="auto"  style="margin-left:100px;">
                <p>
                <p style="margin-left:100px;">NOVOSTI</p>
                <h3 style="margin-left:100px;">Novi Samsung Galaxy S21 Ultra</h3>
                <a href="s21ultra.php" class="button2" style="margin-left:100px;">Pročitaj više</a>
                <p>

                    <br>
                    <br>
                    <img src="images/iphone13kamera.jpg" width="400px" height="auto" style="margin-left:100px;">
                <p>
                <p style="margin-left:100px;">NOVOSTI</p>
                <h3 style="margin-left:100px;">Fantastični iPhone 13 u prodaji</h3>
                <a href="iPhone13.php" class="button2" style="margin-left:100px;">Pročitaj više</a>
                <p>
                <p>
                <img src="images/poco1.png" width="400px" height="auto" style="margin-left:100px;">
                <p style="margin-left:100px;">NOVOSTI</p>
                <h3 style="margin-left:100px;">Zvijezda mjeseca je napokon tu</h3>
                <a href="xiaomipoco.php" class="button2" style="margin-left:100px;">Pročitaj više</a> <p>
                    
                   
            </div>
          <br>

        </div>

    </div>

    <br><br>
    <div class="container">
        <div class="row">
            <h4>Pogledajte ostalu ponudu u našem web shopu!</h4><br><br>
            <div class="col-sm">
                <img src="images/iphone13.jpg" width="370px" height="400px" class="gallery-item" alt="gallery">
                <p id="ponuda1">iPhone 13</p>
            </div>
            <div class="col-sm">
                <img src="images/galaxya52s.jpg" width="370px" height="360px" class="gallery-item" alt="gallery">
                <p id="ponuda2">Samsung Galaxy A52s 5G</p>
            </div>
            <div class="col-sm">
                <img src="images/motorolae7.png" width="200px" height="350px" class="gallery-item" alt="gallery"
                    id="motog7">
                <p id="ponuda3">Motorola E7 siva</p>
            </div>

        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm">
                <img src="images/huaweip40lite.jpg" width="380px" height="400px" class="gallery-item" alt="gallery">
                <p id="ponuda1">Huawei P40 lite zeleni</p>
            </div>
            <div class="col-sm">
                <img src="images/a03s.jpg" width="370px" height="360px" class="gallery-item" alt="gallery">
                <p id="ponuda2">Samsung Galaxy A03s crni</p>
            </div>
            <div class="col-sm">
                <img src="images/xiamoi11.jpg" width="350px" height="350px" class="gallery-item" alt="gallery">
                <p id="ponuda3X">Xiaomi Mi 11 lite sivi</p>
            </div>

        </div>
        <a href="HTML/ponuda.php"><button class="button-24" role="button">Pogledaj ponudu <i
                    class="fas fa-arrow-right"></i></button></a>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="gallery-modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <!--<h5 class="modal-title" id="exampleModalLabel">Modal title</h5> !-->
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <img src="images/iphone13.jpg" width="370px" height="400px" class="modal-img" alt="modal img">
                </div>
            </div>
        </div>
    </div>


    <br><br><br><br>


    <div class="container">
        <div class="row">
            <div class="col-lg-4 mb-4">
                <div class="card h-100">
                    <h4 class="card-header">
                        <i class="fas fa-undo"></i>
                        Siguran i jednostavan povrat
                    </h4>
                    <div class="card-body">
                        <p class="card-text">
                            Sve narudžbe možete vratiti u roku od 14 dana od primitka.
                        </p>
                    </div>
                </div>

            </div>
            <div class="col-lg-4 mb-4">
                <div class="card h-100">
                    <h4 class="card-header">
                        <i class="fas fa-truck"></i>
                        Brza i besplatna dostava
                    </h4>
                    <div class="card-body">
                        <p class="card-text">
                            Besplatna dostava na području Hrvatske, 3-5 radnih dana.
                    </div>
                </div>
            </div>

            <div class="col-lg-4 mb-4">
                <div class="card h-100">
                    <h4 class="card-header">
                        <i class="fas fa-certificate"></i>
                        Sigurno plaćanje
                    </h4>
                    <div class="card-body">
                        <p class="card-text">Tvoji osobni i financijski podaci zaštićeni su sigurnosnim protokolom s
                            256-bitnom šifrom (SSL).
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>

    <!-- Back to top button -->
    <a class="back-to-top"><i class="fas fa-arrow-up"></i></a>

    <!--https://w3schoolweb.com/bootsrap5-footer-with-social-media-icons/-->
    <section class="footer">
        <div class="container">
            <div class="footer__content">
                <div class="footer__heading">
                    <h2>MobilMania</h2>
                </div>
                <p class="mb-0">Copyright &copy; 2021 mato3089@gmail.com</p>

                <ul class="social__media">
                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></li>
                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                </ul>
            </div>
        </div>
    </section>

</body>

</html>