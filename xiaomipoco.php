<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MobilMania WebShop</title>

    <!--BOOTSTRAP-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"> </script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!---->
    <script src="https://kit.fontawesome.com/4a15fc06a1.js" crossorigin="anonymous"></script>

    <link href="style/css.css" rel="stylesheet">
    <link href="style/onama.css" rel="stylesheet">

    <script src="js/javascript.js">
    
  </script>


<body>

<?php include('navbar.php'); ?>
    <br>
    <br><br><br>
    <div class="container">
        <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Xiaomi mobiteli</li>
            </ol>
        </nav>
    </div><br>


<div class="container-fluid">
    <br><br><br>
    <h1>&nbsp;&nbsp;&nbsp;O uređaju</h1>
    <br>
    <div class="row bg-dark">
        <div class="col-lg-6">
            <br>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="images/poco1.png" width="500px" height="350px" class="rounded"> 
        </div>
        <div class="col-lg-6">
            <br>
            <br>
            <br>
            <br>
            <br>
            <h2 style="color: white;"><b>Sve što ti treba i više!</b></h2>
            <p style="color: white;">Qualcomm® Snapdragon™ 860 procesor neće te iznevjeriti čak ni kada igraš grafički zahtjevne igre na visokim postavkama. Uživaj u velikim brzinama i online igranju bez ometanja.</p> 
        </div>        
    </div>
    <div class="row bg-dark">
        <div class="col-lg-6">
            <br>
            <br>
            <br>
            &nbsp;&nbsp;&nbsp; <h2 style="color: white; margin-left: 40px;"><b>Brzina pohrane koja nikog ne ostavlja ravnodušnim</b></h2>
            <p style="color: white; margin-left: 40px;">S poboljšanom tehnologijom memorije, POCO X3 Pro nudi puno bolju brzinu učitavanja igara i aplikacija bez čekanja.</p>
        </div>
        <div class="col-lg-6"><br><br>
            &nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;  <img src="images/poco2.png" width="600px" height="400px" class="rounded">
        </div>
    </div>
    <div class="row bg-dark">
        <br><br><br>
        <div class="col-lg-6"><br><br>
            <br>
            <img src="images/poco3.png" width="600px" height="350px" class="rounded" style="margin-left: 30px;"> 
        </div>
        <div class="col-lg-6">
            <br>
            <br>
            <br>
            <br>
            <br>
            <h2 style="color: white;"><b>Zvučni efekti koji će te odvesti u svijet gaminga</b></h2>
            <p style="color: white;">Igraj igrice, uživaj u preciznosti zvuka svakog pokreta.</p> 
        </div>
    </div>
    <div class="row bg-dark">
        <div class="col-lg-6">
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <h2 style="color: white; margin-left: 40px;"><b>Najčvršće staklo dosad</b></h2>
            <p style="color: white; margin-left: 40px;">Dizajnirano kako bi se manje grebalo i za zaštitu sprijeda i straga,    ovo je najtvrđe Gorilla Glass staklo na pametnom telefonu.</p>
        </div>
        <div class="col-lg-6"><br><br><br>
            &nbsp;&nbsp;&nbsp;    &nbsp;&nbsp;&nbsp; <img src="images/poco4.png" width="550px" height="360px" class="rounded"><br><br>
        </div>
    </div>
    <div class="row bg-dark">
        <br><br><br>
        <div class="col-lg-6"><br><br>
            <br>
            <img src="images/poco5.png" width="600px" height="380px" class="rounded" style="margin-left: 30px;"> 
        </div>
        <div class="col-lg-6">
            <br>
            <br>
            <br>
            <br>
            <br>
            <h2 style="color: white;"><b>Zoomiraj za više zabave</b></h2>
            <p style="color: white;">Glavna kamera od 64 MP sa Sony IMX682 omogućuje ti uvijek jasne i detaljne izvorne slike čak i kada je zoom na 1%</p> 
        </div>
    </div>
</div>
<br><br>

<div class="container">
    <div class="col-lg-12">
    <h1>Tehničke specifikacije</h1>        
    <table class="table table-light table-hover">
      <tbody>
        <tr>
            <td>Podržane mreže</td>
            <td>GSM 850, LTE 1800, LTE 2600, 900, 1800, 1900, UMTS 850, UMTS 900, UMTS 1900, UMTS 2100, LTE 800 MHz</td>
          </tr>
        <tr>
          <td>Prijenos podataka</td>
          <td>GPRS, HSDPA 42.2, EDGE, LTE Cat20 2000 Mbps DL, 5G, UMTS, HSUPA, HSUPA 5.76
        </td>
        </tr>
        <tr>
          <td>Povezivost</td>
          <td>Bluetooth, NFC, Sinkronizacija s racunalom, USB kabel, WLAN, GPS</td>
        </tr>
        <tr>
            <td>Poruke</td>
            <td>SMS, MMS, E-mail klijent</td>
        </tr>
        <tr>
            <td>Zaslon</td>
            <td>6.67" FHD+ LCD DotDisplay</td>
        </tr>
        <tr>
            <td>Kamera</td>
            <td>Četverostruka glavna (48 Mpx + 8 Mpx + 2 Mpx + 2 Mpx) + Selfie (20 Mpx)</td>
        </tr>
        <tr>
            <td>Memorija telefona</td>
            <td>Radna memorija 6GB, korisnička i sistemska memorija 128GB/td>
        </tr>
        <tr>
            <td>Procesor</td>
            <td>Qualcomm; Kryo; 485</td>
        </tr>
        <tr>
            <td>Dimenzije uređaja</td>
            <td>165.3 x 76.8 x 9.4 mm</td>
        </tr>
        <tr>
            <td>Težina</td>
            <td>214 g</td>
        </tr>
      </tbody>
    </table>
    </div>
</div>
<br>
  
<a class="back-to-top"><i class="fas fa-arrow-up"></i></a>

      <!--https://w3schoolweb.com/bootsrap5-footer-with-social-media-icons/-->
      <section class="footer">
        <div class="container">
          <div class="footer__content">
            <div class="footer__heading">
              <h2>MobilMania</h2>
            </div>
            <p class="mb-0">Copyright &copy; 2021 mato3089@gmail.com</p>
       
            <ul class="social__media">
              <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></li>
              <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
            </ul>
          </div>
        </div>
      </section>
      
</body>
</html> 