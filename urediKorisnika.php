<?php

include 'connection.php';
session_start();

$errorMsg = "";

$id = $_GET['id']; // get id through query string

$qry = mysqli_query($conn, "SELECT * FROM korisnici WHERE id='$id'");

$data = mysqli_fetch_array($qry);

$username = $email = $role= "";
$isFilled = true;

if(isset($_POST['uredi'])) {

    if (empty($_POST['username'])) {
        $errorMsg .= "Ime obavezno. ";
        $isFilled = false;
    }
    else $username = $_POST['username'];

    if (empty($_POST['email'])) {
        $errorMsg .= "Email obavezan. ";
        $isFilled = false;
    }
    else $email = $_POST['email'];

    if (empty($_POST['role'])) {
        $errorMsg .= "Uloga obavezna. ";
        $isFilled = false;
    }
    else $role = $_POST['role'];


    if($isFilled) {
        $sql = "UPDATE korisnici SET username='$username', email='$email', role='$role' WHERE id='$data[id]'";
        if (mysqli_query($conn, $sql)) {
            header('Location: upravljanjeKorisnika.php');
        } else {
            echo "Error: " . $sql . ":-" . mysqli_error($conn);
        }
        mysqli_close($conn);
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MobilMania WebShop</title>

    <!--BOOTSTRAP-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"> </script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!---->
    <script src="https://kit.fontawesome.com/4a15fc06a1.js" crossorigin="anonymous"></script>

    <link href="style/css.css" rel="stylesheet">

    <script src="js/javascript.js"></script>

    <script src="include.js"></script>
</head>

<body class="mx-auto">
<?php include('navbar.php'); ?>
<br><br><br>
<br><br><br><br>
<style>
a{text-decoration:none;}
</style>

<header class="mb-5"><h1 class="text-center">Uređivanje korisnika</h1></header>
 
<main>
    <div class="container-fluid text-center ">
        <div class="row">
             <div class="col-md-9 mx-auto ">
             <form method="post" action="" class="row">
                        <div class="row mb-3">
                            <div class="col-md-6">
                                <label for="username" class="form-label">Username</label>
                                <input type="text" name="username" class="form-control" value="<?php echo $data['username'] ?>"autofocus>
                            </div>
                            <div class="col-md-6">
                                <label for="email" class="form-label">Email</label>
                                <input type="text" name="email" class="form-control" value="<?php echo $data['email'] ?>">
                            </div>
                            <div class="col-md-6"><br> 
                                <label for="role" class="form-label">Uloga(admin/korisnik)</label>
                                <select class="form-select" name="role">
                                    <option value="admin" <?php if($data['role']=="admin") echo 'selected="selected"'; ?>>Admin</option>
                                    <option value="korisnik" <?php if($data['role']=="korisnik") echo 'selected="selected"'; ?>>Korisnik</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12 mt-3 mb-3">
                            <button type="submit" name="uredi" class="button-24">&nbsp;&nbsp;&nbsp;Uredi&nbsp;&nbsp;&nbsp;</button>
                            <button type="reset" class="button-24">Resetiraj</button>
                            <a href="upravljanjeKorisnika.php" class="button-24">Povratak</a>
                        </div>
                        <div class="my-2">
                            <p id="errorMsg"><?php echo $errorMsg ?></p>
                        </div>
                    </form>
            </div>
        </div>
    </div>
</main><br> 
<section class="footer">
  <div class="container">
    <div class="footer__content">
      <div class="footer__heading">
        <h2>MobilMania</h2>
      </div>
      <p class="mb-0">Copyright &copy; 2021 mato3089@gmail.com</p>
 
      <ul class="social__media">
        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
        <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></li>
        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
        <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
      </ul>
    </div>
  </div>
</section>
</body>
</html>