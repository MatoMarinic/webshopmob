<?php
session_start();
include_once 'connection.php';

$errorMsg = "";

$id = $_GET['id']; // get id through query string

$qry = mysqli_query($conn,"SELECT * FROM products WHERE id='$id'");

$data = mysqli_fetch_array($qry);

$quantity = $price = null;
$manufacturer = $model = $tip = $image = $zaslon = $procesor = $ram = $baterija = "";
$isFilled = true;

if (isset($_POST['uredi'])) { 
    $manufacturer = $_POST['manufacturer'];
    $model = $_POST['model'];
    $price = $_POST['price'];
    $quantity = $_POST['quantity'];
    $tip = $_POST['tip'];
    $image = $_POST['image'];
    $zaslon = $_POST['zaslon'];
    $procesor = $_POST['procesor'];
    $ram = $_POST['ram'];
    $baterija = $_POST['baterija'];

    $errorMsg = "";

    if (empty($_POST['manufacturer'])) {
        $errorMsg .= "Proizvodjac obavezan. ";
        $isFilled = false;
    }
    else $manufacturer = $_POST['manufacturer'];

    if (empty($_POST['model'])) {
        $errorMsg .= "Model obavezan";
        $isFilled = false;
    }
    else $model = $_POST['model'];

    if (empty($_POST['zaslon'])) {
        $errorMsg .= "Zaslon obavezan";
        $isFilled = false;
    }
    else $zaslon = $_POST['zaslon'];

    if (empty($_POST['procesor'])) {
        $errorMsg .= "Procesor obavezan";
        $isFilled = false;
    }
    else $procesor = $_POST['procesor'];

    if (empty($_POST['ram'])) {
        $errorMsg .= "Ram obavezan";
        $isFilled = false;
    }
    else $ram = $_POST['ram'];

    if (empty($_POST['baterija'])) {
        $errorMsg .= "Baterija obavezna";
        $isFilled = false;
    }
    else $baterija = $_POST['baterija'];

    if(empty($_POST['price']) or $_POST['price'] <= 0.00) {
        $errorMsg .= "Cijena mora biti veća od 0.";
        $isFilled = false;
    }
    else $price = $_POST['price'];
        
    if(empty($_POST['quantity']) or $_POST['quantity'] <= 0) {
        $errorMsg .= "Količina mora biti veća od 0!";
        $isFilled = false;
    }
    else $quantity = $_POST['quantity'];

    if (empty($_POST['tip'])) {
        $errorMsg .= "Tip obavezan";
        $isFilled = false;
    }
    else $tip = $_POST['tip'];

    if (empty($_POST['image'])) {
        $errorMsg .= "Slika obavezan";
        $isFilled = false;
    }
    else $image = $_POST['image'];


    if($isFilled) {
        $sql = "UPDATE products set manufacturer='$manufacturer', model='$model', zaslon='$zaslon', procesor='$procesor', ram='$ram', baterija='$baterija', price='$price', quantity='$quantity', tip='$tip', image='$image' WHERE id='$data[id]'";
        if (mysqli_query($conn, $sql)) {
            header('Location: upravljanjeProizvodima.php');
        } else {
            echo "Error: " . $sql . ":-" . mysqli_error($conn);
        }
        mysqli_close($conn);
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MobilMania WebShop</title>

    <!--BOOTSTRAP-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"> </script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!---->
    <script src="https://kit.fontawesome.com/4a15fc06a1.js" crossorigin="anonymous"></script>

    <link href="style/css.css" rel="stylesheet">

    <script src="js/javascript.js"></script>

    <script src="include.js"></script>
</head>
<style>
a{
    text-decoration: none;
}
</style>
<body>
<?php include('navbar.php'); ?>
<br><br><br><br><br><br><br>
    <header class="mb-5"><h1 class="text-center">Dodavanje proizvoda</h1></header>
    <main>
        <div class="container-fluid mx-auto text-center">
            <div class="row">
                <div class="col-md-9 mx-auto">
                    <form method="post" action="" class="row">
                        <div class="row mb-3">
                            <div class="col-md-4">
                                <label for="manufacturer" class="form-label">Proizvodjac</label>
                                <select class="form-select" name="manufacturer">
                                    <option value="Samsung" <?php if($data['manufacturer']=="Samsung") echo 'selected="selected"';?>>Samsung</option>
                                    <option value="iPhone" <?php if($data['manufacturer']=="iPhone") echo 'selected="selected"';?>>iPhone</option>
                                    <option value="Xiaomi" <?php if($data['manufacturer']=="Xiaomi") echo 'selected="selected"';?>>Xiaomi</option>
                                    <option value="Motorola" <?php if($data['manufacturer']=="Motorola") echo 'selected="selected"';?>>Motorola</option>
                                    <option value="Huawei" <?php if($data['manufacturer']=="Huawei") echo 'selected="selected"';?>>Huawei</option>
                                    <option value="OnePlus" <?php if($data['manufacturer']=="OnePlus") echo 'selected="selected"';?>>OnePlus</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label for="model" class="form-label">Model</label>
                                <input type="text" value="<?php echo $data['model'] ?>" class="form-control" name="model">
                            </div>


                            <div class="col-md-3">
                                <label for="zaslon" class="form-label">Zaslon</label>
                                <input type="text" value="<?php echo $data['zaslon'] ?>" class="form-control" name="zaslon">
                            </div>

                            <div class="col-md-3">
                                <label for="procesor" class="form-label">Procesor</label>
                                <input type="text" value="<?php echo $data['procesor'] ?>" class="form-control" name="procesor">
                            </div>

                            <div class="col-md-3">
                                <label for="ram" class="form-label">RAM</label>
                                <input type="text" value="<?php echo $data['ram'] ?>" class="form-control" name="ram">
                            </div>

                            <div class="col-md-3">
                                <label for="baterija" class="form-label">Baterija</label>
                                <input type="text" value="<?php echo $data['baterija'] ?>" class="form-control" name="baterija">
                            </div>

                            <div class="col-md-4">
                                <label for="tip" class="form-label">Tip</label>
                                <select class="form-select" name="tip">
                                    <option value="Novo" <?php if($data['tip']=="Novo") echo 'selected="selected"'; ?> >Novo</option>
                                    <option value="Popularno"<?php if($data['tip']=="Popularno") echo 'selected="selected"'; ?>>Popularno</option>
                                    <option value="NULL" <?php if($data['tip']=="NULL") echo 'selected="selected"'; ?>>Ništa</option>
                                </select>
                            </div>

                            <div class="col-md-3">
                                <label for="price" class="form-label">Cijena</label>
                                <input type="number" name="price" step="any" value="<?php echo $data['price'] ?>" class="form-control">
                            </div>
                            <div class="col-md-3">
                                <label for="quantity" class="form-label">Količina</label>
                                <input type="number" value="<?php echo $data['quantity'] ?>" class="form-control" name="quantity">
                            </div>
                            <div class="col-md-3">
                                <label for="image" class="form-label">Slika</label>
                                <input type="file" value="<?php echo $data['image'] ?>" class="form-control" name="image">
                            </div>
                        </div>
                        
                        <div class="col-12 mt-3 mb-3">
                            <button type="submit" name="uredi" class="button-24">&nbsp;&nbsp;&nbsp;Uredi&nbsp;&nbsp;&nbsp;</button>
                            <button type="reset" class="button-24">Resetiraj</button>
                            <a href="upravljanjeProizvodima.php" class="button-24">Povratak</a>
                        </div>
                        <div class="my-2">
                            <p id="errorMsg"><?php echo $errorMsg ?></p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>
</header>
<br><br><br><br><br><br>
<section class="footer">
  <div class="container">
    <div class="footer__content">
      <div class="footer__heading">
        <h2>MobilMania</h2>
      </div>
      <p class="mb-0">Copyright &copy; 2021 mato3089@gmail.com</p>
 
      <ul class="social__media">
        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
        <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></li>
        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
        <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
      </ul>
    </div>
  </div>
</section>

</body>
</html>