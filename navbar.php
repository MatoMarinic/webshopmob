<head>
    <script>

         $(document).ready(function(){
             if(!localStorage.getItem('artikli')) $('.kosarica span').text(0);
             else $('.kosarica span').text(parseInt(localStorage.getItem('artikli')));            
            })
    </script>
</head>


<nav class="navbar fixed-top navbar-expand-sm navbar-dark bg-dark">
    <div class="container">
        <a class="navbar-brand" href="index.php">MobilMania WebShop</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-bs-toggle="collapse"
            data-bs-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="collapsibleNavbar">

            <ul class="navbar-nav me-auto">

                <li class="nav-item">
                    <a class="nav-link" href="aboutUs.php">O nama</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="kontakt.php">Kontakt</a>
                </li>
               
              <?php if(isset($_SESSION['username'])){
                    echo '<li class="nav-item">
                            <a class="nav-link" href="logout.php">Odjavi me</a>
                        </li>';
                }else{
                   echo '<li class="nav-item">
                        <a class="nav-link" href="login.php"><i class="far fa-user"></i>&nbsp;   Login</a>
                    </li>';
                }?>
                <?php if(isset($_SESSION['role'])){
                    
                if($_SESSION['role'] == 'admin'){
                    echo '<li class="nav-item">
                        <a class="nav-link" href="dashboard.php">Admin dashboard</a>
                        </li>';?>
                </ul>
                <?php
                echo '<p style="color:red; margin-top:10px"> Pozdrav, '.$_SESSION['username'].'! </p>';
                }
            else if($_SESSION['role'] == 'korisnik'){
                echo '<p style="color:white; margin-top:10px"> Pozdrav, '.$_SESSION['username'].'! </p>';
            }
            }
            ?>

            <!--SHOPPING CART-->
            <?php
            if (isset($_SESSION['loggedin']) && $_SESSION['loggedin']) {?>
            <div class="d-flex justify-content-end">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="cart.php" class="dropdown-toggle" data-toggle="dropdown" role="button"
                            aria-expanded="false"> <i class="fas fa-shopping-cart"></i></a>
                        <ul class="dropdown-menu dropdown-cart" role="menu">
                            <li>
                                <span class="item-right">
                                    <button class="btn btn-danger fa fa-close"></button>
                                </span>
                            </li>
                    </li>
                </ul>
            </div>
            <?php
            }
            ?>
            <!--SHOPPING CART END-->
        </div>
    </div>
</nav>
