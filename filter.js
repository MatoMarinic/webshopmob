$(document).ready(function () {
	function filterItems() {
		let index = 0;
		let obj = $("#product_" + index);

		let minPrice = $("#hidden_minimum_price").val();
		let maxPrice = $("#hidden_maximum_price").val();
		let proizvodjacElements = $(".proizvodjac");
		let checkedProizvodjac = [];

		for (e of proizvodjacElements) {
			let value = e.getAttribute("value");
			let checked = e.checked;
			if (checked) {
				checkedProizvodjac.push(value);
			}
		}

		while (obj[0] != undefined) {
			let cijenaProizvoda = parseFloat(
				$("#product_" + index + "_price")[0].innerHTML,
			);
			let proizvod = $("#product_" + index + "_manufacturer")[0]
				.innerHTML;

			if (
				cijenaProizvoda < maxPrice &&
				cijenaProizvoda > minPrice &&
				(checkedProizvodjac.includes(proizvod) ||
				checkedProizvodjac.length == 0)
			) {
				obj.removeClass("d-none");
			} else {
				obj.addClass("d-none");
			}
			index++;
			obj = $("#product_" + index);
		}
	}

	$(".proizvodjac").change(function () {
		filterItems();
	});

	$("#price_range").slider({
		range: true,
		min: 0,
		max: 15000,
		values: [0, 15000],
		step: 1,
		slide: function (event, ui) {
			$("#price_show").html(ui.values[0] + " - " + ui.values[1]);
			$("#hidden_minimum_price").val(ui.values[0]);
			$("#hidden_maximum_price").val(ui.values[1]);
			filterItems();
		},
	});
});
