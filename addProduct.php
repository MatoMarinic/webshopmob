<?php
 session_start();
include_once 'connection.php';

$errorMsg = "";

$quantity = $price = null;
$manufacturer = $model = $tip = $image = $zaslon = $procesor = $ram = $baterija = "";
$isFilled = true;

if (isset($_POST['spremi'])) { 
    $manufacturer = $_POST['manufacturer'];
    $model = $_POST['model'];
    $price = $_POST['price'];
    $quantity = $_POST['quantity'];
    $tip = $_POST['tip'];
    $image = $_POST['image'];
    $zaslon = $_POST['zaslon'];
    $procesor = $_POST['procesor'];
    $ram = $_POST['ram'];
    $baterija = $_POST['baterija'];
    $errorMsg = "";


    if (empty($_POST['manufacturer'])) {
        $errorMsg .= "Proizvodjac obavezan. ";
        $isFilled = false;
    }
    else $manufacturer = $_POST['manufacturer'];

    if (empty($_POST['model'])) {
        $errorMsg .= "Model obavezan";
        $isFilled = false;
    }
    else $model = $_POST['model'];

    if (empty($_POST['zaslon'])) {
        $errorMsg .= "Zaslon obavezan. ";
        $isFilled = false;
    }
    else $zaslon = $_POST['zaslon'];

    if (empty($_POST['procesor'])) {
        $errorMsg .= "Procesor obavezan. ";
        $isFilled = false;
    }
    else $procesor = $_POST['procesor'];

    if (empty($_POST['ram'])) {
        $errorMsg .= "RAM obavezan. ";
        $isFilled = false;
    }
    else $ram = $_POST['ram'];

    if (empty($_POST['baterija'])) {
        $errorMsg .= "Baterija obavezna. ";
        $isFilled = false;
    }
    else $baterija = $_POST['baterija'];

    if(empty($_POST['price']) or $_POST['price'] <= 0.00) {
        $errorMsg .= "Cijena mora biti veća od 0.";
        $isFilled = false;
    }
    else $price = $_POST['price'];
        
    if(empty($_POST['quantity']) or $_POST['quantity'] <= 0) {
        $errorMsg .= "Količina mora biti veća od 0!";
        $isFilled = false;
    }
    else $quantity = $_POST['quantity'];
    
    if (empty($_POST['tip'])) {
        $errorMsg .= "Tip obavezan";
        $isFilled = false;
    }
    else $tip = $_POST['tip'];

    if (empty($_POST['image'])) {
        $errorMsg .= "Slika obavezan";
        $isFilled = false;
    }
    else $image = $_POST['image'];


    if($isFilled) { 
        $query = "INSERT INTO products (manufacturer, model, zaslon, procesor, ram, baterija, price, quantity, tip, image) VALUES ('$manufacturer', '$model', '$zaslon', '$procesor', '$ram', '$baterija', '$price', '$quantity', '$tip', '$image')";
        if (mysqli_query($conn, $query)) {
            header('Location: upravljanjeProizvodima.php');
        } else {
            echo "Error: " . $query . ":-" . mysqli_error($conn);
        }
        mysqli_close($conn);
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MobilMania WebShop</title>

    <!--BOOTSTRAP-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"> </script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!---->
    <script src="https://kit.fontawesome.com/4a15fc06a1.js" crossorigin="anonymous"></script>

    <link href="style/css.css" rel="stylesheet">

    <script src="js/javascript.js"></script>

    <script src="include.js"></script>
</head>
<style>
a{
    text-decoration: none;
}
</style>
<body>
<?php include('navbar.php'); ?>
<br><br><br><br><br><br><br>
    <header class="mb-5"><h1 class="text-center">Dodavanje proizvoda</h1></header>
    <main>
        <div class="container mx-auto text-center">
            <div class="row">
                <div class="col-md-9 mx-auto">
                    <form method="post" action="" class="row">
                        <div class="row mb-3">
                            <div class="col-md-4">
                                <label for="manufacturer" class="form-label">Proizvodjac</label>
                                <select class="form-select" name="manufacturer">
                                    <option value="Samsung">Samsung</option>
                                    <option value="iPhone">iPhone</option>
                                    <option value="Xiaomi">Xiaomi</option>
                                    <option value="Motorola">Motorola</option>
                                    <option value="Huawei">Huawei</option>
                                    <option value="OnePlus">OnePlus</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label for="model" class="form-label">Model</label>
                                <input type="text" class="form-control" name="model">
                            </div>
                            <div class="col-md-3">
                                <label for="zaslon" class="form-label">Zaslon</label>
                                <input type="text" class="form-control" name="zaslon">
                            </div>
                            <div class="col-md-3">
                                <label for="procesor" class="form-label">Procesor</label>
                                <input type="text" class="form-control" name="procesor">
                            </div>
                            <div class="col-md-3">
                                <label for="ram" class="form-label">RAM</label>
                                <input type="text" class="form-control" name="ram">
                            </div>
                            <div class="col-md-3">
                                <label for="baterija" class="form-label">Baterija</label>
                                <input type="text" class="form-control" name="baterija">
                            </div>

                            <div class="col-md-4">
                                <label for="tip" class="form-label">Tip</label>
                                <select class="form-select" name="tip">
                                    <option value="Novo" >Novo</option>
                                    <option value="Popularno">Popularno</option>
                                    <option value="NULL">Ništa</option>
                                </select>
                            </div>

                            <div class="col-md-3">
                                <label for="price" class="form-label">Cijena</label>
                                <input type="number" name="price" step="any" value=0 class="form-control">
                            </div>
                            <div class="col-md-3">
                                <label for="quantity" class="form-label">Količina</label>
                                <input type="number" value=1 class="form-control" name="quantity">
                            </div>
                            <div class="col-md-3">
                                <label for="image" class="form-label">Slika</label>
                                <input type="file" class="form-control" name="image">
                            </div>
                        </div>
                        
                        <div class="col-12 mt-3 mb-3">
                            <button type="submit" name="spremi" class="button-24">&nbsp;&nbsp;&nbsp;Dodaj&nbsp;&nbsp;&nbsp;</button>
                            <button type="reset" class="button-24">Resetiraj</button>
                            <a href="upravljanjeProizvodima.php" class="button-24">Povratak</a>
                        </div>
                        <div class="my-2">
                            <p id="errorMsg"><?php echo $errorMsg ?></p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>
</header>
<br><br><br><br><br><br>
<section class="footer">
  <div class="container">
    <div class="footer__content">
      <div class="footer__heading">
        <h2>MobilMania</h2>
      </div>
      <p class="mb-0">Copyright &copy; 2021 mato3089@gmail.com</p>
 
      <ul class="social__media">
        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
        <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></li>
        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
        <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
      </ul>
    </div>
  </div>
</section>

</body>
</html>