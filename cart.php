<?php
include_once 'connection.php';
session_start();

if (!empty($_SESSION['cart'])) {
    $whereIn = implode(',', $_SESSION['cart']);

    $sqlId = "SELECT * FROM products WHERE id IN ($whereIn)";
} else header("Location: index.php");

$cartPrice = 0;
$cartQuantity = 0;

foreach ($_SESSION['cart'] as &$val) {
    $sql = "SELECT * FROM products WHERE id='$val'";
    $q = mysqli_query($conn, $sql);

    while ($line = mysqli_fetch_array($q, MYSQLI_ASSOC)) {
        $cartPrice += $line['price'];
        $cartQuantity++;
    }
}

$sesijaId = session_id();

$errorMsg = "";

$quantity = $price = "";

if (isset($_POST['buy'])) {
    $query = "INSERT INTO kosarica (sesija_id, quantity, price) VALUES ('$sesijaId', '$cartQuantity','$cartPrice')";

    if (mysqli_query($conn, $query)) {
        unset($_SESSION['cart']);
        header("Location: cartBuy.php");
    } else {
        echo "Error: " . $query . ":-" . mysqli_error($conn);
    }
    mysqli_close($conn);
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MobilMania WebShop</title>

    <!--BOOTSTRAP-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"> </script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <link rel="stylesheet" href="//cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">

    <script src="//cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>


    <!---->
    <script src="https://kit.fontawesome.com/4a15fc06a1.js" crossorigin="anonymous"></script>

    <link href="style/css.css" rel="stylesheet">

    <script src="js/javascript.js"></script>

    <script src="addToCart.js"></script>

    <script src="include.js"></script>
</head>

<body>
    <?php include('navbar.php'); ?>

    <br><br><br><br><br>
    <header class="mb-5">
        <h1 class="text-center">Košarica</h1>
    </header>
    <main>
        <p id="dohvatiId"></p>
        <div class="container-fluid">
            <div class="row">
                <div class="mx-auto p-5 my-5">
                    <form action="" method="post">
                        <table class="table table-light table-hover" id="tablica" style="max-height: 100px; height:100%;">
                            <thead>
                                <tr>
                                    <th scope="col">ID</th>
                                    <th scope="col">Proizvođač</th>
                                    <th scope="col">Model</th>
                                    <th scope="col">Količina</th>
                                    <th scope="col">Cijena(kn)</th>
                                    <th scope="col">Ukloni</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($_SESSION['cart'])) {
                                    $sql = $sqlId;
                                    $q = mysqli_query($conn, $sql);
                                    $freqs = array_count_values($_SESSION['cart']);
                                    $koliko = "";
                                    $ukupno = 0;
                                    if ($q) {
                                        while ($line = mysqli_fetch_array($q, MYSQLI_ASSOC)) {
                                            if (isset($freqs[$line['id']])) $koliko = $freqs[$line['id']];
                                            echo '
                                            <tr>
                                                <td>' . $line['id'] . '</td>
                                                <td>' . $line['manufacturer'] . '</td>
                                                <td>' . $line['model'] . '</td>
                                                <td>' . $koliko . '</td>
                                                <td>' . $line['price'] * $koliko . '</td>
                                                <td><a class="btn btn-danger btn-sm" href="deleteFromCart.php?id=' . $line['id'] . '">Ukloni</a>
                                                
                                                </td>
                                            </tr>';

                                            $ukupno += $line['price'] * $koliko;
                                        }
                                    }

                                    echo '<tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>Ukupno: ' . $ukupno . ' kn</td>
                                            <td></td>
                                        </tr>';
                                } ?>
                            </tbody>
                        </table>


                        <button type="submit" class="btn btn-success btn-sm" name="buy">Kupi</button>

                    </form>
                </div>
            </div>
        </div>
    </main>
    <section class="footer">
        <div class="container">
            <div class="footer__content">
                <div class="footer__heading">
                    <h2>MobilMania</h2>
                </div>
                <p class="mb-0">Copyright &copy; 2021 mato3089@gmail.com</p>

                <ul class="social__media">
                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></li>
                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                </ul>
            </div>
        </div>
    </section>

</body>

</html>
